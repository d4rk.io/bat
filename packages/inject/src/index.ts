import "reflect-metadata"
import { Container, injectable, inject } from "inversify"
import Conf from "conf"
import { nanoid } from 'nanoid'
import getDecorators from "inversify-inject-decorators"
import { provide, fluentProvide, buildProviderModule } from "inversify-binding-decorators"
import { isConditionalExpression } from "typescript"

const container = new Container()
const { lazyInject } = getDecorators(container)

const RS = Symbol.for('RS')
interface RS {
    r(a)
}

const OS = Symbol.for('OS')
interface OS {
    r(a)
}

const IS = Symbol.for('IS')
interface IS {
    r(a)
}

const DS = Symbol.for('DS')
interface DS {
    d(a)
}

const AS = Symbol.for('AS')
interface AS {
    d(a)
}

@injectable()
class CRS implements RS {
    @lazyInject(OS) os: OS
    @lazyInject(IS) is: IS

    r(a) {
        if (a === 'c') {
            console.log('rs -> c')
            this.os.r(a)
            return
        }

        if (a === 's') {
            console.log('rs -> s')
            this.is.r(a)
            return
        }

        throw 'ur'
    }
}

@injectable()
class COS implements OS {
    @lazyInject(DS) ds: DS

    r(a) {
        console.log('os')
        this.ds.d(a)
    }
}

@injectable()
class CIS implements IS {
    r(a) {
        console.log('is')
    }
}

@injectable()
class CDS implements DS {
    @lazyInject(AS) as: AS

    d(a) {
        console.log('ds')
        this.as.d(a)
    }
}

@injectable()
class CAS implements AS {
    @lazyInject(RS) rs: RS

    d(a) {
        console.log('as')
        this.rs.r('s')
    }
}

container.bind<RS>(RS).to(CRS).inSingletonScope()
container.bind<OS>(OS).to(COS).inSingletonScope()
container.bind<IS>(IS).to(CIS).inSingletonScope()
container.bind<DS>(DS).to(CDS).inSingletonScope()
container.bind<AS>(AS).to(CAS).inSingletonScope()

container.get<RS>(RS).r('c')