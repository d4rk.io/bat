import { GlobalID } from "@d4rkio/activity-pub/src/types";
import { ApRequest } from "@d4rkio/activity-pub/src/types/request";
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream";

export interface Backend {
    get(target: GlobalID, request: ApRequest): Promise<ActivityStreamObject>
    send(target: GlobalID, object: ActivityStreamObject,  request: ApRequest): Promise<GlobalID>
}