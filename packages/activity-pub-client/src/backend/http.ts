import { GlobalID } from "@d4rkio/activity-pub/src/types";
import { Backend } from "./interface";
import fetch from 'node-fetch'
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream";
import { ApRequest } from "@d4rkio/activity-pub/src/types/request";

export class HttpBackend implements Backend {
    async get(target: GlobalID, request: ApRequest): Promise<ActivityStreamObject> {
        const response = await fetch(target.toString(), {
            headers: {
                authorization: 'id ' + request.identity.toString()
            }
        })

        return response.json()
    }

    async send(target: GlobalID, object: ActivityStreamObject, request: ApRequest): Promise<GlobalID> {
        const response = await fetch(target.toString(), {
            body: JSON.stringify(object),
            method: 'POST',
            headers: {
                'Content-Type': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
                authorization: 'id ' + request.identity.toString()
            }
        })

        return GlobalID.create(response.headers.get('Location'))
    }

    async createActor(name: string, url: string = 'http://localhost:9000'): Promise<GlobalID> {
        const response = await fetch(url + '/dev/register/'+ name, {
            method: 'POST',
            headers: {
               'Content-Type': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
            },
        })

        return GlobalID.create(response.headers.get('Location'))
    }
}