import { GlobalID } from "@d4rkio/activity-pub/src/types";
import { Backend } from "./backend";
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream";
import { ApRequest } from "@d4rkio/activity-pub/src/types/request";

export class Client {
    private actroid: GlobalID
    private backend: Backend

    constructor(backend: Backend, actorid: GlobalID) {
        this.backend = backend
        this.actroid = actorid
    }

    async get(target: GlobalID): Promise<ActivityStreamObject> {
        return await this.backend.get(target, {identity: this.actroid})
    }

    async send(object: ActivityStreamObject, request: ApRequest) {
        const inboxid = await this.getOutboxID()
        return await this.backend.send(inboxid, object, {...request, identity: this.actroid})
    }

    async getInbox(): Promise<ActivityStreamObject> {
        const inboxid = await this.getInboxID()
        return await this.backend.get(inboxid, {identity: this.actroid})
    }

    async getFollowerCollection(actorid?: GlobalID) {
       return await this.get(await this.getFollowerCollectionID(actorid))
    }

    async getLikesCollection(actorid?: GlobalID) {
        if (actorid === undefined) {
            actorid = this.actroid
        }

        return await this.get(await this.getLikedCollectionID(actorid))
    }

    async like(activity: ActivityStreamObject) {
        await this.send({
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Like",
            "to": [activity.actor as string],
            "actor": this.actroid.toString(),
            "object": activity.id
        }, {})
    }

    async follow(actorid: GlobalID) {
        await this.send({
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Follow",
            "to": [actorid.toString()],
            "actor": this.actroid.toString(),
            "object": actorid.toString()
        }, {})
    }

    private async getInboxID(): Promise<GlobalID> {
        const actor = await this.get(this.actroid)
        return GlobalID.create(actor['inbox'] as string)
    }

    private async getOutboxID(): Promise<GlobalID> {
        const actor = await this.get(this.actroid)
        return GlobalID.create(actor['outbox'] as string)
    }

    private async getLikedCollectionID(actroid: GlobalID): Promise<GlobalID> {
        const actor = await this.get(actroid)
        const likedid = actor['liked'] as string

        if (likedid === undefined) {
            throw new Error('Actor dose not have a liked collection')
        }

        return GlobalID.create(likedid)
    }

    async getFollowerCollectionID(actorid?: GlobalID): Promise<GlobalID> {
        if (actorid === undefined) {
            actorid = this.actroid
        }

        const actor = await this.get(actorid)
        const likedid = actor['followers'] as string

        return GlobalID.create(likedid)
    }
}