import { GlobalID } from "@d4rkio/activity-pub/src/types";
import { sleep } from "@d4rkio/activity-pub/src/utils";
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream";
import { Client } from "../src";
import { HttpBackend } from "../src/backend/http";

describe('standart intro example', () => {
    let client1: Client
    let client2: Client
    let actorid1: GlobalID
    let actorid2: GlobalID
    test('new', async () => {
        let backend = new HttpBackend()
        actorid1 = await backend.createActor('client1')
        client1 = new Client(backend, actorid1)

        backend = new HttpBackend()
        actorid2 = await backend.createActor('client2')
        client2 = new Client(backend, actorid2)
    })

    test('client1 resolve outbox', async () => {
        //@ts-ignore getOutboxID is private
        const outboxID = await client1.getOutboxID()

        expect(outboxID.toString()).toContain('outbox')

        // result is not used client resolve inbox if it needs to
        // this is just a rest if it works
    })

    let note: ActivityStreamObject 
    test('client1 send note to client 2 and 3', async () => {
        note = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Note",
            "to": [actorid2.toString()],
            "attributedTo": actorid1.toString(),
            "content": "Say, did you finish reading that book I lent you?"
        }

        await client1.send(note, {})
    })

    let note1id: string
    test('client2 check inbox', async () => { 
        await sleep(20) //async delivery may not be done now
        const inbox = await client2.getInbox()

        const items = inbox.items as ActivityStreamObject[]
        expect(items.length).toStrictEqual(1)

        const note1 = items[0].object as ActivityStreamObject
        note1id = note1.id
        delete note1.id

        expect(note1).toStrictEqual(note)
        expect(note1id).not.toBeUndefined()
    })

    let activity2: ActivityStreamObject
    test('client2s awnser', async () => {
        activity2 = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Create",
            "to": [actorid1.toString()],
            "actor": actorid2.toString(),
            "object": {
                "type": "Note",
                "attributedTo": actorid2.toString(),
                "to": [actorid1.toString()],
                "inReplyTo": note1id,
                "content": "<p>Argh, yeah, sorry, I'll get it back to you tomorrow.</p><p>I was reviewing the section on register machines, since it's been a while since I wrote one.</p>"
            }
        }

        await client2.send(activity2, {})
    })

    let activity3: ActivityStreamObject
    test('client1 check inbox', async () => {
        await sleep(20) //async delivery may not be done now
        const inbox = await client1.getInbox()

        const items = inbox.items as ActivityStreamObject[]
        expect(items.length).toStrictEqual(1)

        activity3 = items[0] as ActivityStreamObject
        const activity2id = activity3.id

        const note2 =  activity3.object as ActivityStreamObject
        const note2id = note2.id

        delete activity3.id
        delete note2.id

        expect(activity3).toStrictEqual(activity2)
        expect(activity2id).not.toBeUndefined()
        expect(note2id).not.toBeUndefined()
    })

    test('client1 like activity 3', async () => {
        await client1.like(activity3)
    })

    test('get likes', async () => {
        await sleep(50)
        const likes = await client1.getLikesCollection()

        const items = likes.items as ActivityStreamObject[]

        expect(items.length).toStrictEqual(1)
        expect(items[0].id).not.toBeUndefined()
    })
})