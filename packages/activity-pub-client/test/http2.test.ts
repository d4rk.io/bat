import { GlobalID } from "@d4rkio/activity-pub/src/types";
import { sleep } from "@d4rkio/activity-pub/src/utils";
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream";
import { Client } from "../src";
import { HttpBackend } from "../src/backend/http";

let client1: Client
let client2: Client
let client3: Client
let actorid1: GlobalID
let actorid2: GlobalID
let actorid3: GlobalID
test('new', async () => {
    let backend = new HttpBackend()
    actorid1 = await backend.createActor('client1')
    client1 = new Client(backend, actorid1)

    backend = new HttpBackend()
    actorid2 = await backend.createActor('client1')
    client2 = new Client(backend, actorid2)

    backend = new HttpBackend()
    actorid3 = await backend.createActor('clinet3')
    client3 = new Client(backend, actorid3)
})

test('actor 1 and 3 follow actor 2', async () => {
    await client1.follow(actorid2)
    await client3.follow(actorid2)
})

test('actor 2 check followers', async () => {
    await sleep(200) // side effects are handeld async
    const followers = await client2.getFollowerCollection()

    console.log(followers)

    const items = followers.items as ActivityStreamObject[]

    expect(items.length).toStrictEqual(2)
    expect(items[0].id).toStrictEqual(actorid1.toString())
    expect(items[1].id).toStrictEqual(actorid3.toString())
})

let note: ActivityStreamObject
test('actor 2 write to followers', async () => {
    const followersid = client2.getFollowerCollectionID()

    note = {
        "@context": "https://www.w3.org/ns/activitystreams",
        "type": "Note",
        "to": [(await client2.getFollowerCollectionID()).toString()],
        "attributedTo": actorid2.toString(),
        "content": "Hello ~~World~~ Followers!"
    }

    await client2.send(note, {})
})

test('actor 1 and 3 get inbox', async () => {
    await sleep(50) //async delivery may not be done now
    const inbox1 = await client1.getInbox()
    const inbox3 = await client3.getInbox()

    const items1 = inbox1.items as ActivityStreamObject[]
    const items3 = inbox3.items as ActivityStreamObject[]

    expect(items1.length).toStrictEqual(2)
    expect(items3.length).toStrictEqual(2)

    const note1 = items1[1].object as ActivityStreamObject
    const note3 = items3[1].object as ActivityStreamObject
    
    delete note1.id
    delete note3.id

    expect(note1).toStrictEqual(note)
    expect(note3).toStrictEqual(note)
})