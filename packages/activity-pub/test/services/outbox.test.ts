import nano from "nano"
import { DeliveryService, OutboxService, SideEffectService } from "@d4rkio/activity-pub/src/services"
import { RequestError } from "@d4rkio/activity-pub/src/utils"
import { GlobalID, LocalID } from "@d4rkio/activity-pub/src/types"
import { createConfig } from "@d4rkio/activity-pub/src/config"
import { bindContainer } from "@d4rkio/activity-pub/src/container"
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream"

const config = createConfig()
config.set('ActivityPub.Services.CollectionService.dbName', 'collections_outbox_test')
config.set('ActivityPub.Services.ObjectService.dbName', 'objects_outbox_test')
config.set('ActivityPub.ID.HTTP.url', 'http://ap.test')
config.set('ActivityPub.Services.ObjectService.baseID', '/object')
config.set('ActivityPub.Services.CollectionService.baseID', '/object')

describe('outbox', () => {
    afterAll(async () => {
        const db = nano(config.get('ActivityPub.couchdb.url') as string).db
        await db.destroy(config.get('ActivityPub.Services.ObjectService.dbName') as string).catch(() => {})
        await db.destroy(config.get('ActivityPub.Services.CollectionService.dbName') as string).catch(() => {})
    })

    const mockSideEffectService = {
        execute: jest.fn().mockReturnValue(Promise.resolve())
    }

    const mockDeliveryService = {
        deliver: jest.fn().mockReturnValue(Promise.resolve())
    }

    const container = bindContainer(config)
    container.rebind<SideEffectService>(SideEffectService).toConstantValue(mockSideEffectService)
    container.rebind<DeliveryService>(DeliveryService).toConstantValue(mockDeliveryService)
    const outboxService = container.get<OutboxService>(OutboxService)

    beforeEach(() => {
        jest.clearAllMocks()
    })

    let outboxID: LocalID
    let outboxID2: LocalID
    let outboxOwner = GlobalID.create('outboxOwne40')
    let outboxOwner2 = GlobalID.create('outvoxOwner42')
    describe('create', () => {
        test('create', async () => {
            const outboxGID = await outboxService.create(outboxOwner)
            outboxID = LocalID.fromGlobalID(outboxGID.toString())
        })

        test('createA2', async () => {
            const outboxGID = await outboxService.create(outboxOwner2)
            outboxID2 = LocalID.fromGlobalID(outboxGID)
        })
    })

    describe('recive', () => {
        test('recive', async () => {
            const obj = {'type': 'Create', message: "hello world"}
            const result = await outboxService.receive(outboxID, obj, {identity: outboxOwner})

            expect(result.toString().startsWith('http://ap.test/object/')).toBeTruthy()

            const expObj = {
                'type': 'Create',
                message: "hello world",
                'id': result.toString()
            }

            expect(mockSideEffectService.execute).toBeCalledWith(expObj, {federated: false, identity: outboxOwner, owner: outboxOwner})
            expect(mockDeliveryService.deliver).toBeCalledWith(expObj, {})
        })

        test('not an activity', async () => {
            const obj = {message: "hello world", '@context': 'mycontest'}
            const result = await outboxService.receive(outboxID, obj, {identity: outboxOwner})

            expect(result.toString().startsWith('http://ap.test/object/')).toBeTruthy()

            const expObj = {
                '@context': obj['@context'],
                'type': 'Create',
                'id': result.toString(),
                'object': obj,
                'actor': outboxOwner.toString()
            }


            //get call x from mock eg. toBeCalledWith(x)
            const executeMockCall = mockSideEffectService.execute.mock.calls[0][0]
            const deliveryMockCall = mockSideEffectService.execute.mock.calls[0][0]

            //drop sub object id
            delete executeMockCall.object.id
            delete executeMockCall.object.id

            //check with out random id
            expect(executeMockCall).toStrictEqual(expObj)
            expect(deliveryMockCall).toStrictEqual(expObj)
        })

        test('not id', async () => {
            const obj = {'type': 'Create', message: "hello world"}
            await expect(
                outboxService.receive(outboxID, obj, {identity: undefined})
            ).rejects.toThrow(
                new RequestError('Not Authorized', 401)
            )

            expect(mockSideEffectService.execute).not.toBeCalled()
            expect(mockDeliveryService.deliver).not.toBeCalled()
        })

        test('no valid id', async () => {
            const obj = {'type': 'Create', message: "world, hello"}
            await expect(
                outboxService.receive(outboxID, obj, {identity: GlobalID.create('nottheowner')})
            ).rejects.toThrow(
                new RequestError('Not Allowed; Only the owner can publish to its outbox', 405)
            )

            expect(mockSideEffectService.execute).not.toBeCalled()
            expect(mockDeliveryService.deliver).not.toBeCalled()
        })

        test('outbox not found', async () => {
            const obj = {'type': 'Create', message: "hello world"}
            await expect(
                outboxService.receive(LocalID.create('notAnOutboxID'), obj, {identity: GlobalID.create('nottheowner')})
            ).rejects.toThrow(
                new RequestError('Outbox not found', 404)
            )

            expect(mockSideEffectService.execute).not.toBeCalled()
            expect(mockDeliveryService.deliver).not.toBeCalled()
        })
    })

    describe('get', () => {
        test('not found', async () => {
            await expect(
                outboxService.get(LocalID.create('notAnOutboxID'), {identity: undefined})
            ).rejects.toThrow(
                new RequestError('Outbox not found', 404)
            )
        })

        test('get', async () => {
            const outbox = await outboxService.get(outboxID, {identity: undefined})
            const itemes: ActivityStreamObject[] = outbox.items as ActivityStreamObject[]
            delete outbox.items
            
            expect(outbox).toStrictEqual({
                '@context': 'https://www.w3.org/ns/activitystreams',
                'type': 'Collection',
                attributedTo: outboxOwner.toString()
            })

            expect(itemes.length).toStrictEqual(2)

            const id1 = itemes[0]['id']
            const id2 = itemes[1]['id']
            delete itemes[0]['id']
            delete itemes[1]['id']

            expect(itemes[0]).toStrictEqual({
                'type': 'Create',
                message: 'hello world',
            })

            const innterObject = itemes[1].object as ActivityStreamObject

            expect(innterObject.id).not.toBeUndefined()
            delete innterObject.id

            expect(itemes[1]).toStrictEqual({
                '@context': 'mycontest',
                'type': 'Create',
                object: {message: "hello world", '@context': 'mycontest'},
                actor: 'outboxOwne40',
            })

            expect(id1.startsWith('http://ap.test/object/')).toBeTruthy()
            expect(id2.startsWith('http://ap.test/object/')).toBeTruthy()
        })
    })
})