import { GetService, implementation } from "@d4rkio/activity-pub/src/services"
import { GlobalID } from "@d4rkio/activity-pub/src"
import { createConfig } from "@d4rkio/activity-pub/src/config"
import { bindContainer } from "@d4rkio/activity-pub/src/container"

const config = createConfig()
config.set('ActivityPub.Services.DeliveryService.maxTries', 3)
config.set('ActivityPub.Services.DeliveryService.timeout', 1000)
config.set('ActivityPub.Services.DeliveryService.backoffTime', 100)

describe('delivery service', () => {
    const container = bindContainer(config)
    container.bind<implementation.ConcreatDeliveryService>(implementation.ConcreatDeliveryService).toSelf()

    describe('deliver', () => {
        test('with audience of 3', async () => {
            const mockedGetSerice = {
                get: jest.fn().mockImplementation(() => Promise.reject()),
                getAll: jest.fn().mockResolvedValueOnce(new Map([
                    [GlobalID.create('bensid'), {'type': 'Person','inbox': 'bensinbox'}],
                    [GlobalID.create('alicesid'), {'type': 'Person','inbox': 'alicesinbox'}],
                    [GlobalID.create('evesid'), {'type': 'Person','inbox': 'evesinbox'}]
                ]))
            }
            container.rebind<GetService>(GetService).toConstantValue(mockedGetSerice)
            const deliveryService = container.get<implementation.ConcreatDeliveryService>(implementation.ConcreatDeliveryService)
            const callback = jest.fn().mockImplementation(async () => {})

            await deliveryService.resolve([GlobalID.create("bensid"), GlobalID.create("alicesid"), GlobalID.create("evesid")], callback)

            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["bensid", "alicesid", "evesid"]), {})
            expect(callback).toBeCalledWith(GlobalID.createMultiple(['bensinbox', 'alicesinbox', 'evesinbox']))
        })

        test('with collections', async () => {
            const mockedGetSerice = {
                get: jest.fn().mockImplementation(() => Promise.reject()),
                getAll: jest.fn().mockResolvedValueOnce(new Map([
                    [GlobalID.create('bensid'), {'type': 'Person','inbox': 'bensinbox'}],
                    [GlobalID.create('alicesid'), {'type': 'Person','inbox': 'alicesinbox'}],
                    [GlobalID.create('gdi1'), {'type': 'Collection','items': ['uid1', 'uid2']}],
                    [GlobalID.create('gdi2'), {'type': 'Collection','items': ['uid3', 'uid4', 'uid5', 'uid2']}]
                ])).mockResolvedValueOnce(new Map([
                    [GlobalID.create('uid1'), {'type': 'Person','inbox': 'uid1inbox'}],
                    [GlobalID.create('uid2'), {'type': 'Person','inbox': 'uid2inbox'}],
                    [GlobalID.create('uid3'), {'type': 'Person','inbox': 'uid3inbox'}],
                    [GlobalID.create('uid4'), {'type': 'Person','inbox': 'uid4inbox'}],
                    [GlobalID.create('uid5'), {'type': 'Person','inbox': 'uid5inbox'}],
                    [GlobalID.create('uid2'), {'type': 'Person','inbox': 'uid2inbox'}],
                ]))
            }
            container.rebind<GetService>(GetService).toConstantValue(mockedGetSerice)
            const deliveryService = container.get<implementation.ConcreatDeliveryService>(implementation.ConcreatDeliveryService)
            const callback = jest.fn().mockImplementation(async () => {})

            await deliveryService.resolve([GlobalID.create("bensid"), GlobalID.create("alicesid"), GlobalID.create("gid1"), GlobalID.create("gid2")], callback)

            expect(mockedGetSerice.getAll).toBeCalledWith([GlobalID.create("bensid"), GlobalID.create("alicesid"), GlobalID.create("gid1"), GlobalID.create("gid2")], {})
            expect(mockedGetSerice.getAll).toBeCalledWith([GlobalID.create("uid1"), GlobalID.create("uid2"), GlobalID.create("uid3"), GlobalID.create("uid4"), GlobalID.create('uid5'), GlobalID.create("uid2")], {})
            expect(callback).toBeCalledWith([GlobalID.create('bensinbox'), GlobalID.create('alicesinbox'),GlobalID.create("uid1inbox"),GlobalID.create("uid2inbox"),GlobalID.create("uid3inbox"),GlobalID.create("uid4inbox"),GlobalID.create("uid5inbox")])
        })

        test('with fail hard', async () => {
            const mockedGetSerice = {
                get: jest.fn().mockImplementation(() => Promise.reject()),
                getAll: jest.fn().mockResolvedValueOnce(new Map([
                    [GlobalID.create('failid'), undefined],
                    [GlobalID.create('goodid'), {'type': 'Collection','items': ['uid1', 'uid2']}],
                ])).mockResolvedValueOnce(new Map([
                    [GlobalID.create('uid1'), {'type': 'Person','inbox': 'uid1inbox'}],
                    [GlobalID.create('uid2'), {'type': 'Person','inbox': 'uid2inbox'}],
                ])).mockResolvedValue(new Map([
                    [GlobalID.create('failid'), undefined]
                ]))
            }
            container.rebind<GetService>(GetService).toConstantValue(mockedGetSerice)
            const deliveryService = container.get<implementation.ConcreatDeliveryService>(implementation.ConcreatDeliveryService)
            const callback = jest.fn().mockImplementation(async () => {})

            await deliveryService.resolve([GlobalID.create("failid"), GlobalID.create("goodid")], callback)

            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["failid", "goodid"]), {})
            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["uid1", "uid2"]), {})
            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["failid"]), {})
            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["failid"]), {})
            expect(callback).toBeCalledWith(GlobalID.createMultiple(['uid1inbox', 'uid2inbox']))
        })

        test('with fail recover', async () => {
            const mockedGetSerice = {
                get: jest.fn().mockImplementation(() => Promise.reject()),
                getAll: jest.fn().mockResolvedValueOnce(new Map([
                    [GlobalID.create('failid'), undefined],
                    [GlobalID.create('goodid'), {'type': 'Collection','items': ['uid1', 'uid2']}],
                ])).mockResolvedValueOnce(new Map([
                    [GlobalID.create('uid1'), {'type': 'Person','inbox': 'uid1inbox'}],
                    [GlobalID.create('uid2'), {'type': 'Person','inbox': 'uid2inbox'}],
                ])).mockResolvedValueOnce(new Map([
                    [GlobalID.create('failid'), undefined]
                ])).mockResolvedValueOnce(new Map([
                    [GlobalID.create('failid'), {'type': 'Collection','items': ['uid3', 'uid4']}]
                ])).mockResolvedValueOnce(new Map([
                    [GlobalID.create('uid3'), {'type': 'Person','inbox': 'uid3inbox'}],
                    [GlobalID.create('uid4'), {'type': 'Person','inbox': 'uid4inbox'}],
                ]))
            }
            container.rebind<GetService>(GetService).toConstantValue(mockedGetSerice)
            const deliveryService = container.get<implementation.ConcreatDeliveryService>(implementation.ConcreatDeliveryService)
            const callback = jest.fn().mockImplementation(async () => {})

            await deliveryService.resolve([GlobalID.create("failid"), GlobalID.create("goodid")], callback)

            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["failid", "goodid"]), {})
            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["uid1", "uid2"]), {})
            expect(callback).toBeCalledWith(GlobalID.createMultiple(['uid1inbox', 'uid2inbox']))
            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["failid"]), {})
            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["failid"]), {})
            expect(mockedGetSerice.getAll).toBeCalledWith(GlobalID.createMultiple(["uid3", "uid4"]), {})
            expect(callback).toBeCalledWith(GlobalID.createMultiple(["uid3inbox", "uid4inbox"]))
        })
    })
})