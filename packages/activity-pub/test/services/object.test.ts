import nano from "nano"
import { ObjectService } from "@d4rkio/activity-pub/src/services"
import { InvalidObjectAPError, NotFoundAPError} from "@d4rkio/activity-pub/src/utils"
import { LocalID } from "@d4rkio/activity-pub/src/types"
import { createConfig } from "@d4rkio/activity-pub/src/config"
import { bindContainer } from "@d4rkio/activity-pub/src/container"
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream"
 
const config = createConfig()
config.set('ActivityPub.Services.ObjectService.dbName', 'objects_test')
config.set('ActivityPub.ID.HTTP.url', 'http://ap.test')
config.set('ActivityPub.Services.ObjectService.baseID', '/object')


describe('object service', () => {
    afterAll(async () => {
        const db = nano(config.get('ActivityPub.couchdb.url') as string).db
        await db.destroy(config.get('ActivityPub.Services.ObjectService.dbName') as string).catch(() => {})
    })

    const container = bindContainer(config)
    const objectsService = container.get<ObjectService>(ObjectService)

    let couchId: LocalID
    let object: ActivityStreamObject
    describe('create', () => {
        test('throws if id pressent', async () => {
            await expect(
                objectsService.create({'id': 'https://social.example/u/alice'})
            ).rejects.toThrow(
                new InvalidObjectAPError({toMuch: 'id'})
            )
        })

        test('create object, check id', async () => {
            object = {'keyT': 'value1', key1: 'value2'}
            const clone = Object.assign({}, object)

            const result = await objectsService.create(object)
            
            const id = result['id']
            delete result['id']

            //check object was not modifies
            expect(object).toStrictEqual(clone)

            //check object is the same object
            expect(result).toStrictEqual(object)
            
            //check id ...
            expect(id.startsWith('http://ap.test/object/')).toBeTruthy()

            couchId = LocalID.fromGlobalID(id)
        })

        test('create 2 more objects', async () => {
            await objectsService.create( {'keyT': 'value2', key2: 'valueA2'})
            await objectsService.create( {'keyT': 'value3', key3: 'valueA3'})
        })
    })

    describe('get', () => {
        test('get previusly created object', async () => {
            const result = await objectsService.get(couchId, {})

            object['id'] = 'http://ap.test/object/' + couchId
            expect(result).toStrictEqual(object)
        })

        test('get unknow object', async () => {
            await expect(
                objectsService.get(LocalID.create('bc155646fbd80d502bae741b9702da2d'), {})
            ).rejects.toThrow(
                new NotFoundAPError('object')
            )
        })
    })

    describe('update', () => {
        test('update', async () => {
            const object = await objectsService.create({'oldkey': 'avalue', 'perskey': 'oldvalue'})

            const id: LocalID = LocalID.fromGlobalID(object['id'])

            delete object.oldkey
            object.newkey = 'somevalue'
            object.perskey = 'newvalue'

            const clone = Object.assign({}, object)
            await objectsService.update(object)
            expect(object).toStrictEqual(clone)

            const result = await objectsService.get(id, {})
            expect(result).toStrictEqual({
                'id': object['id'],
                perskey: 'newvalue',
                newkey: 'somevalue',
            })
        })
    })
})