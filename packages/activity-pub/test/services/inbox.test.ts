import nano from "nano"
import { RequestError } from "@d4rkio/activity-pub/src/utils"
import { GlobalID, LocalID } from "@d4rkio/activity-pub/src/types"
import { createConfig } from "@d4rkio/activity-pub/src/config"
import { bindContainer } from "@d4rkio/activity-pub/src/container"
import { InboxService, SideEffectService } from "@d4rkio/activity-pub/src"

const config = createConfig()
config.set('ActivityPub.Services.CollectionService.dbName', 'collections_inbox_test')

describe('inbox', () => {
    afterAll(async () => {
        const db = nano(config.get('ActivityPub.couchdb.url') as string).db
        await db.destroy(config.get('ActivityPub.Services.CollectionService.dbName') as string).catch(() => {})
    })

    const mockSideEffectService = {
        execute: jest.fn().mockReturnValue(Promise.resolve())
    }

    const container = bindContainer(config)
    container.rebind<SideEffectService>(SideEffectService).toConstantValue(mockSideEffectService)
    const inboxService = container.get<InboxService>(InboxService)

    beforeEach(() => {
        jest.clearAllMocks()
    })
    
    let collectionID: LocalID
    let collectionID2: LocalID
    const ownerID = GlobalID.create("ownerid007")
    const ownerID2 = GlobalID.create("ownerid008")
    describe('create', () => {
        test('create', async () => {
            const id = await inboxService.create(ownerID)
            collectionID = LocalID.fromGlobalID(id)
        })

        test('createA2', async () => {
            const id = await inboxService.create(ownerID2)
            collectionID2 = LocalID.fromGlobalID(id)
        })
    })

    describe('recive', () => {
        test('write message', async () => {
            await inboxService.receive(collectionID2, {'type': 'Create', 'id': 'zneedsid', 'ba': 'z first activity'}, {})

            expect(mockSideEffectService.execute).toBeCalled()
        })

        test('write second message', async () => {
            await inboxService.receive(collectionID2, {'type': 'Create', 'id': 'aneedsid', 'ab': 'a second activity'}, {identity: undefined})

            expect(mockSideEffectService.execute).toBeCalled()
        })

        test('not an activity', async () => {
            await expect(
                inboxService.receive(LocalID.create('notvalid'), {'id': 'an id', no: 'type'}, {})
            ).rejects.toThrow(
                new RequestError('Only activies can be delivert to an inbox', 400)
            )
        })

        test('unknow inbox', async () => {
            await expect(
                inboxService.receive(LocalID.create('notvalid'), {'type': 'Create', 'id': 'aneedsid', 'ab': 'a second activity'}, {})
            ).rejects.toThrow(
                new RequestError('Outbox not found', 404)
            )
        })

        test('without id', async () => {
            await expect(
                inboxService.receive(LocalID.create('collectionID2'), {'type': 'Create', 'ab': 'a second activity'}, {})
            ).rejects.toThrow(
                new RequestError('Invalid object', 400)
            )
        })
    })

    describe('get', () => {
        test('get without an identity', async () => {
            await expect(
                inboxService.get(LocalID.create('notvalid'), {identity: undefined})
            ).rejects.toThrow(
                new RequestError('Not Authorized', 401)
            )
        })

        test('get non existent inbox', async () => {
            await expect(
                inboxService.get(LocalID.create('nonexistentid'), {identity: GlobalID.create('someidbutcanbetheownerofno')})
            ).rejects.toThrow(
                new RequestError('Outbox not found', 404)
            )
        })

        test('get with wrong ownerid', async () => {
            await expect(
                inboxService.get(collectionID, {identity: GlobalID.create('nottheownersid')})
            ).rejects.toThrow(
                new RequestError('Not Allowed; Only the owner can acces its inbox', 405)
            )
        })

        test('get inbox', async () => {
            const inbox = await inboxService.get(collectionID, {identity: ownerID})
            
            expect(inbox).toStrictEqual(    {
                '@context': 'https://www.w3.org/ns/activitystreams',
                'type': 'Collection',
                attributedTo: ownerID.toString(),
                items: []
            })
        })

        test('get inbox', async () => {
            const inbox = await inboxService.get(collectionID2, {identity: ownerID2})
            
            expect(inbox).toStrictEqual(    {
                '@context': 'https://www.w3.org/ns/activitystreams',
                'type': 'Collection',
                attributedTo: ownerID2.toString(),
                items: [
                    {'type': 'Create', 'id': 'zneedsid', 'ba': 'z first activity'},
                    {'type': 'Create', 'id': 'aneedsid', 'ab': 'a second activity'}
                ]
            })
        })
    })
})