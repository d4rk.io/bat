import nano from "nano"
import { CollectionService } from "@d4rkio/activity-pub/src/services"
import { InvalidObjectAPError, NotFoundAPError } from "@d4rkio/activity-pub/src/utils"
import { GlobalID, LocalID } from "@d4rkio/activity-pub/src/types"
import process from 'process'
import { bindContainer } from "@d4rkio/activity-pub/src/container"
import { createConfig } from "@d4rkio/activity-pub/src/config"
import { ActivityStreamObject } from "@d4rkio/activity-pub/src/utils/activity-stream"

const config = createConfig()
config.set('ActivityPub.Services.CollectionService.dbName', 'collections_test')

describe('collection service', () => {
    afterAll(async () => {
        const db = nano(config.get('ActivityPub.couchdb.url') as string).db
        await db.destroy(config.get('ActivityPub.Services.CollectionService.dbName') as string).catch(() => {})
    })

    const container = bindContainer(config)
    const collectionService = container.get<CollectionService>(CollectionService)

    let collectionID: LocalID
    const ownerID = GlobalID.create("ownerid007")
    test('create', async () => {
        collectionID = await collectionService.create(ownerID)
    })

    let collectionID2: LocalID
    const ownerID2 = GlobalID.create("ownerid008")
    test('create', async () => {
        collectionID2 = await collectionService.create(ownerID2)
    })

    test('getOwner', async () => {
        const result = await collectionService.getOwner(collectionID)
        expect(result).toStrictEqual(ownerID)
    })

    let someobject: ActivityStreamObject
    let sameobject: ActivityStreamObject
    describe('push', () => {
        test('push object', async () => {
            someobject = {'id': 'someid', 'somekey': 'somevaole'}
            try {
                await collectionService.push(collectionID, someobject)
            } catch (e) {
                console.log(e)
            }
        })

        test('push the same object 2 times', async () => {
            sameobject = {'id': 'sameid', 'samekey': 'samevalue'}
            await collectionService.push(collectionID, sameobject)
            await collectionService.push(collectionID, sameobject)
        })

        test('push without id', async () => {
            const withoutid = {'notanid': 'notanidvlaue', anotherKey: 42}
            await expect(
                collectionService.push(collectionID, withoutid)
            ).rejects.toThrow(
                new InvalidObjectAPError({missing: 'id'})
            )
        })

        test('push to non existen collection', async () => {
            const obj = {'id': 'anidvalue', anykey: 'anyvalue'}
            await expect(
                collectionService.push(LocalID.create('bc155646fbd80d502bae741b9702da2d'), obj)
            ).rejects.toThrow(
                new NotFoundAPError('collection')
            )
        })
    })

    describe('get', () => {
        test('get collection', async () => {
            const collection = await collectionService.get(collectionID, {})

            expect(collection).toStrictEqual({
                    '@context': 'https://www.w3.org/ns/activitystreams',
                    'type': 'Collection',
                    'attributedTo': ownerID.toString(),
                    'items': [
                        someobject,
                        sameobject,
                        sameobject
                    ]
                }
            )
        })

        test('get second collection', async () => {
            const collection = await collectionService.get(collectionID2, {})

            expect(collection).toStrictEqual({
                    '@context': 'https://www.w3.org/ns/activitystreams',
                    'type': 'Collection',
                    'attributedTo': ownerID2.toString(),
                    'items': []
                }
            )
        })
    })
})