import { activityStream } from "@d4rkio/activity-pub/src/utils"
import { GlobalID } from "@d4rkio/activity-pub/src"


describe('getAudience', () => {
    test('only to as array', () => {
        const obj = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Note",
            "to": ["https://chatty.example/ben/", "https://social.example/alyssa/"],
            "attributedTo": "https://social.example/alyssa/",
            "content": "Say, did you finish reading that book I lent you?"
        }

        const result = activityStream.getAudience(obj)
        expect(result).toStrictEqual([GlobalID.create('https://chatty.example/ben/'), GlobalID.create("https://social.example/alyssa/")])
    })

    test('only to as string', () => {
        const obj = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Note",
            "to": "https://chatty.example/ben/",
            "attributedTo": "https://social.example/alyssa/",
            "content": "Say, did you finish reading that book I lent you?"
        }

        const result = activityStream.getAudience(obj)
        expect(result).toStrictEqual([GlobalID.create('https://chatty.example/ben/')])
    })

    test('every attribute set', () => {
        const obj = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Note",
            "to": "_to_",
            "bto": ["_bto_"],
            "cc": ["_cc_", "_cc1_"],
            "bcc": "_bcc_",
            "audience": ["_audience_"]
        }

        const result = activityStream.getAudience(obj)
        expect(result).toEqual(GlobalID.createMultiple(['_to_', '_bto_', '_cc_', '_cc1_', '_bcc_', '_audience_']))
    })
})