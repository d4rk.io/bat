import { GlobalID } from "./ids";

export interface ApRequest {
    identity?: GlobalID
    owner?: GlobalID
    federated?: boolean
    synchronous?: boolean
}