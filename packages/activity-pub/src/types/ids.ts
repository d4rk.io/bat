import path from 'path/posix'

export class GlobalID {
    private id: string

    private constructor(id: string) {
        this.id = id
    }

    equals(other) {
        return this.toString() === other.toString()
    }

    toString() {
        return this.id
    }

    static create(id: string): GlobalID {
        if (id === undefined) {
            return undefined
        }

        return new GlobalID(id)
    }

    static createMultiple(ids: string[]): GlobalID[] {
        return ids.map(id => this.create(id))
    }
}

export class LocalID {
    static fromGlobalID(id: GlobalID | string): LocalID {
        return new LocalID(path.basename(new URL(id.toString()).pathname))
    }
    private id: string

    private constructor(id: string) {
        this.id = id
    }

    equals(other) {
        return this.toString() === other.toString()
    }

    toString() {
        return this.id
    }

    static create(id: string): LocalID {
        return new LocalID(id)
    }

    static createMultiple(ids: string[]): LocalID[] {
        return ids.map(id => new LocalID(id))
    }
}