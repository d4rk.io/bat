import { NodeObject } from "jsonld";
import { GlobalID } from "../types";

export namespace Types {
    //Activityies
    export const Accept = 'Accept'
    export const TentativeAccept = 'TentativeAccept'
    export const Add = 'Add'
    export const Arrive = 'Arrive'
    export const Create = 'Create'
    export const Delete = 'Delete'
    export const Follow = 'Follow'
    export const Ignore = 'Ignore'
    export const Join = 'Join'
    export const Leave = 'Leave'
    export const Like = 'Like'
    export const Offer = 'Offer'
    export const Invite = 'Invite'
    export const Reject = 'Reject'
    export const TentativeReject = 'TentativeReject'
    export const Remove = 'Remove'
    export const Undo = 'Undo'
    export const Update = 'Update'
    export const View = 'View'
    export const Listen = 'Listen'
    export const Read = 'Read'
    export const Move = 'Move'
    export const Travel = 'Travel'
    export const Announce = 'Announce'
    export const Block = 'Block'
    export const Flag = 'Flag'
    export const Dislike = 'Dislike'
    export const Question = 'Question'
    export const activities = [
        Accept, TentativeAccept, Add, Arrive, Create, Delete, Follow, Ignore,
        Join, Leave, Like, Offer, Invite, Reject, TentativeReject, Remove, 
        Undo, Update, View, Listen, Read, Move, Travel, Announce, Block,
        Flag, Dislike, Question
    ]
    //Actros
    export const Application = 'Application'
    export const Group = 'Group'
    export const Organization = ''
    export const Person = 'Person'
    export const Service = 'Service'
    export const actors = [Application, Group, Organization, Person, Service]
    //Collections
    export const Collection = 'Collection'
    export const OrderedCollection = 'OrderedCollection'
    export const collections= [Collection, OrderedCollection]
}

const targetAudienceAttributes = ['to', 'bto', 'cc', 'bcc', 'audience']

function isObjectOfType(typeCollections: string[]) {
    return function (object: ActivityStreamObject): boolean {
        if (object.type === undefined) {
            return false
        }
    
        return includesAny(typeCollections, object.type)
    }
}

function includesAny(heystack: string[], needles: string | string[]): boolean {
    if (typeof needles === 'string') {
        needles = [needles]
    }

    for (const needle of needles) {
        if (heystack.includes(needle)) {
            return true
        }

        return false
    }
}

export function getAudience(object: ActivityStreamObject): GlobalID[] {
    const audience: GlobalID[]= []
    for (const attribut of targetAudienceAttributes) {
        if (object[attribut] !== undefined) {
            if (typeof object[attribut] === 'string') {
                audience.push(GlobalID.create(object[attribut] as string))
            } else {
                for (const id of object[attribut] as string[]) {
                    audience.push(GlobalID.create(id))
                }
            }
            
        }
    }

    return audience
}

export function toActivity(object: ActivityStreamObject, actor?: GlobalID): ActivityStreamObject {
    if (isActivity(object)) {
        return object
    }

    const activity: ActivityStreamObject = {
        '@context': object['@context'],
        'type': 'Create',
        'object': object,
    }

    for (const attribut of targetAudienceAttributes) {
        if (object[attribut] !== undefined) {
            activity[attribut] = object[attribut]
        }
    }

    if (actor !== undefined) {
        activity.actor = actor.toString()
    }

    return activity
}

export function idEqual(a: ActivityStreamObject | string | GlobalID, b: ActivityStreamObject | string | GlobalID): boolean {
    if (a == undefined || b == undefined) {
        return false
    }

    if (a instanceof GlobalID) {
        a = a.toString()
    }

    if (b instanceof GlobalID) {
        b = b.toString()
    }

    if (typeof a === 'string') {
        if (typeof b !== 'string') {
            b = b.id
        }

        return a === b
    }

    if (typeof b === 'string') {
        a = a.id

        return a === b
    }

    if (a.id === undefined || b.id === undefined) {
        return false
    }

    return a.id == b.id
}

export const isActor = isObjectOfType(Types.actors)
export const isCollection = isObjectOfType(Types.collections)
export const isActivity = isObjectOfType(Types.activities)

export interface ActivityStreamObject extends NodeObject {
    id?: string
    type?: string
}

export interface ActivityStreamActivity extends ActivityStreamObject {
    object?: ActivityStreamObject
    actor?: string
}