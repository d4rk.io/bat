export class RequestError extends Error {
    statuscode: number

    constructor(message: string, statuscode: number = 400) {
        super(message)
        this.statuscode = statuscode
    }
}

export class APError extends Error {
    constructor(message: string) {
        super(message)
    }
}

export class NotFoundAPError extends APError {
    entity: string

    constructor(entity: string) {
        super(`${entity} not found`)

        this.entity = entity
    }
}

export class InvalidObjectAPError extends APError {
    missing?: string
    toMuch?: string
    malformed?: string

    constructor(opts: {missing?: string, toMuch?: string, malformed?: string}) {
        super('Invalid object')

        this.missing = opts.missing
        this.toMuch = opts.toMuch
        this.malformed = opts.malformed
    }
}