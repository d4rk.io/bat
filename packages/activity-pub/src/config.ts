import 'process'
import Conf from "conf";

export function createConfig() {
    return new Conf<Record<string, any>>({
        defaults: {
            ActivityPub: {
                ID: {
                    HTTP: {
                        url: process.env.ActivityPub_ID_HTTP_url || 'http://localhost:9000',
                        protocols: ['https', 'http'],
                    },
                },
                couchdb: {
                    prefix: '',
                    url: process.env.ActivityPub_couchdb_url || 'http://admin:password@eeeeeeeeee:5984',
                },
                Services: {
                    CollectionService: {
                        dbName: 'collection_service',
                        baseID: '/collection',
                    },
                    DeliveryService: {
                        maxTries: 5,
                        timeout: 1000,
                        backoffTime: 20000,
                    },
                    InboxService: {
                        baseID: '/inbox',
                    },
                    ObjectService: {
                        dbName: 'object_service',
                        baseID: '/object',
                    },
                    OutboxService: {
                        baseID: '/outbox',
                    },
                },
            },
        },
    })
}

export const config = createConfig()
config.clear()