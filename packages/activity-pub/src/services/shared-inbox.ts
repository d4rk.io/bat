import { ApRequest } from "../types/request";
import { ActivityStreamObject } from "../utils/activity-stream";

export interface SharedInboxService {
    receive(activity: ActivityStreamObject, request: ApRequest): Promise<void>
}