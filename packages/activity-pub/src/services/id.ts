
import { GlobalID } from '../types';

export const IDService = Symbol.for('IDService')
export interface IDService {
    isThisServersID(id: GlobalID | URL | String): boolean
}