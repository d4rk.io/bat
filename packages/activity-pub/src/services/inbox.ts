
import { GlobalID } from '../types';

import { GetableService } from './get';
import { ReceivableService } from './recive';

export const InboxService = Symbol.for('InboxService')
export interface InboxService extends GetableService, ReceivableService{
    create(ownerID: GlobalID): Promise<GlobalID>
}