import { ActivityStreamObject } from "../utils/activity-stream";


export const DeliveryService = Symbol.for('DeliveryService')
export interface DeliveryService {
    deliver(object: ActivityStreamObject, options: DeliveryOptions): Promise<void>
}

export interface DeliveryOptions {
    onlyLocalDelivery?: boolean
}