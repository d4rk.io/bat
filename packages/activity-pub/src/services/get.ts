import { GlobalID, LocalID } from "../types";
import { ApRequest } from "../types/request";
import { ActivityStreamObject } from "../utils/activity-stream";

export const GetService = Symbol.for('GetService')
export interface GetService {
    getAll(ids: GlobalID[], request: ApRequest): Promise<Map<GlobalID, ActivityStreamObject>>
    get(id: GlobalID, request: ApRequest): Promise<ActivityStreamObject>
    get(prefix: string, id: LocalID, request: ApRequest): Promise<ActivityStreamObject>
}

export interface GetableService {
    get(id: LocalID, request: ApRequest): Promise<ActivityStreamObject>
    IDprefix: string
}