
import { GlobalID } from '../types'
import { GetableService } from './get'
import { ReceivableService } from './recive'

export const OutboxService = Symbol.for('OutboxService')
export interface OutboxService extends GetableService, ReceivableService {
    create(ownerID: GlobalID): Promise<GlobalID>
}