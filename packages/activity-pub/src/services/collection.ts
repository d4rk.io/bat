import { GlobalID, LocalID } from '../types';
import { ActivityStreamObject } from '../utils/activity-stream';
import { GetableService} from './get';

export const CollectionService = Symbol.for('CollectionService') 
export interface CollectionService extends GetableService {
    push(collection: LocalID, object: ActivityStreamObject): Promise<void>
    getOwner(collection: LocalID): Promise<GlobalID>
    create(owner: GlobalID): Promise<LocalID>
}

export interface CollectionGetPotions {
    filter?: Filter
}

export interface Filter {
    onlyPublic?: boolean
    inAudience?: string
}

export interface Collection extends ActivityStreamObject {
    attributedTo: string
    items?: ActivityStreamObject[]
}