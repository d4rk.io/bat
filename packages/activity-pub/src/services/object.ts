import { ActivityStreamObject } from "../utils/activity-stream";
import { GetableService } from "./get";

export const ObjectService = Symbol.for('ObjectService')
export interface ObjectService extends GetableService {
    create(object: ActivityStreamObject): Promise<ActivityStreamObject>
    update(object: ActivityStreamObject): Promise<ActivityStreamObject>
}