import { multiInject } from "inversify";
import { provideSingleton } from "../../container";
import { InvalidObjectAPError } from "../../utils";
import { SideEffectHandler, SideEffectService } from "..";
import { ActivityStreamObject } from "../../utils/activity-stream";
import { ApRequest } from "../../types/request";

@provideSingleton(SideEffectService)
export class ConcreatSideEffectService implements SideEffectService {
    sideEffectHandlers: {[key: string]: SideEffectHandler} = {}

    constructor(@multiInject(SideEffectHandler) sideEffectHandlers: SideEffectHandler[]) {
        for (const sideEffectHandler of sideEffectHandlers) {
            this.sideEffectHandlers[sideEffectHandler.type] = sideEffectHandler
        }
    }

    async execute(object: ActivityStreamObject, request: ApRequest): Promise<void> {
        let type = object.type
        if (type === undefined) {
            throw new InvalidObjectAPError({missing: 'type'})
        }

        const handler = this.sideEffectHandlers[type]
        if (handler === undefined) {
            return
        }

        handler.handle(object, request)
    }
}