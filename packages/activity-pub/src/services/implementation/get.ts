import { GlobalID, LocalID } from "../../types";
import { APError, RequestError } from "../../utils";
import path from 'path/posix'
import { CollectionService, GetableService, GetService, ObjectService, OutboxService, InboxService } from "..";
import { lazyInject, provideSingleton } from "../../container";
import { inject } from "inversify";
import { ActivityStreamObject } from "../../utils/activity-stream";
import { ApRequest } from "../../types/request";
import { IDService } from "../id";
import { ApiClientService } from "../api-client";

@provideSingleton(GetService)
export class ConcreatGetService {
    @inject(CollectionService) private collectionService: GetableService
    @inject(InboxService) private inboxService: GetableService
    @inject(ObjectService) private objectService: GetableService
    @inject(IDService) private idService: IDService
    @lazyInject(ApiClientService) private apiClientService: ApiClientService
    @lazyInject(OutboxService) private outboxService: GetableService


    get(id: GlobalID, request: ApRequest): Promise<ActivityStreamObject>;
    get(prefix: string, id: LocalID, request: ApRequest): Promise<ActivityStreamObject>;
    async get(gIDOrPrefix: GlobalID | string, optionsOrLID?: ApRequest | LocalID, optionsOrNothing?: ApRequest | undefined): Promise<ActivityStreamObject> {
        if (gIDOrPrefix instanceof GlobalID && (optionsOrLID === undefined || !(optionsOrLID instanceof LocalID))) {
            return await this.getWithGlobalID(gIDOrPrefix, optionsOrLID as ApRequest)
        }

        if (typeof gIDOrPrefix === 'string' && optionsOrLID instanceof LocalID) {
            return await this.getWithLocalID(gIDOrPrefix, optionsOrLID, optionsOrNothing)
        }

        throw new Error('Unreachable code')
    }

    async getAll(ids: GlobalID[], request: ApRequest): Promise<Map<GlobalID, ActivityStreamObject>> {
        const results = new Map<GlobalID, ActivityStreamObject>()

        const promises = []
        for (const id of ids) {
            promises.push(
                this.getWithGlobalID(id, request).catch(err => {
                    if (err instanceof APError) {
                        return undefined
                    }

                    throw err
                }).then(object => {
                    results.set(id, object)
                })
            )
        }

        await Promise.all(promises)

        return results
    }

    private async getWithGlobalID(id: GlobalID, request: ApRequest): Promise<ActivityStreamObject> {
        const idAsUrl = new URL(id.toString())

        if (! this.idService.isThisServersID(idAsUrl)) {
            return await this.apiClientService.get(id)
        }

        const prefix = path.dirname(idAsUrl.pathname)
        const lid: LocalID = LocalID.fromGlobalID(id)

        return await this.getWithLocalID(prefix, lid, request)
    }

    private async getWithLocalID(prefix: string, id: LocalID, request: ApRequest): Promise<ActivityStreamObject> {
        switch (prefix) {
            case this.collectionService.IDprefix:
                return await this.collectionService.get(id, request)

            case this.inboxService.IDprefix:
                return await this.inboxService.get(id, request)

            case this.objectService.IDprefix:
                return await this.objectService.get(id, request)

            case this.outboxService.IDprefix:
                return await this.outboxService.get(id, request)
        }
        
        throw new RequestError('prefix unknow', 500)
    }
}