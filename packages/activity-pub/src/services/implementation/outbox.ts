import Conf from 'conf'
import { inject, lazyInject, provideSingleton } from '../../container'
import { GlobalID, LocalID } from '../../types'
import { activityStream, NotFoundAPError, RequestError } from '../../utils'
import { OutboxService, ObjectService, DeliveryService, CollectionService, SideEffectService } from '..'
import { getLogger } from '../../logger'
import { ActivityStreamObject, idEqual } from '../../utils/activity-stream'
import { ApRequest } from '../../types/request'

const logger = getLogger('OutboxService')

@provideSingleton(OutboxService)
export class ConcreatOutboxService implements OutboxService {
    @inject(CollectionService) private collectionService: CollectionService
    @inject(SideEffectService) private sideEffectService: SideEffectService
    @inject(ObjectService) private objectService: ObjectService
    @lazyInject(DeliveryService) private deliveryService: DeliveryService

    IDprefix: string
    private baseUrl: string

    constructor(@inject('Conf') config: Conf) {
        this.IDprefix = config.get('ActivityPub.Services.OutboxService.baseID') as string
        this.baseUrl = config.get('ActivityPub.ID.HTTP.url') as string
    }

    async create(ownerID: GlobalID): Promise<GlobalID> {
        return GlobalID.create(this.baseUrl + this.IDprefix + '/' + await this.collectionService.create(ownerID))
    }

    async get(outbox: LocalID, request: ApRequest): Promise<ActivityStreamObject> {
        request.federated = true

        try {
            return await this.collectionService.get(outbox, request)
        } catch(e) {
            if (! (e instanceof NotFoundAPError)) {
                throw e
            }

            throw new RequestError('Outbox not found', 404)
        }
    }
    
    async receive(outbox: LocalID, object: ActivityStreamObject, request: ApRequest): Promise<GlobalID> {
        request.federated = false

        if (request.identity === undefined) {
            throw new RequestError('Not Authorized', 401)
        }


        try {
            request.owner = await this.collectionService.getOwner(outbox)
        } catch(e) {
            if (! (e instanceof NotFoundAPError)) {
                throw e
            }

            throw new RequestError('Outbox not found', 404)
        }

        if (! idEqual(request.owner, request.identity)) {
            throw new RequestError('Not Allowed; Only the owner can publish to its outbox', 405)
        }

        object = activityStream.toActivity(object, request.identity)

        object = await this.objectService.create(object)

        logger.debug('outbox service recived activity', {outbox: outbox, sender: request.identity, id: object.id})

        //async calls
        const asyncPromise = Promise.all([
            this.sideEffectService.execute(object, request),
            this.deliveryService.deliver(object, {}),
            this.collectionService.push(outbox, object)
        ])

        if (request.synchronous === true) {
            await asyncPromise
        }

        return GlobalID.create(object.id)
    }
}