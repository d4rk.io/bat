import Conf from "conf";
import { inject, provideSingleton } from "../../container";
import { GlobalID } from "../../types";
import { IDService } from "../id";

@provideSingleton(IDService)
export class ConcreatIDService implements IDService {
    private port: string
    private hostname: string
    private protocols: string[]

    constructor(@inject('Conf') config: Conf) {
        const url = new URL(config.get('ActivityPub.ID.HTTP.url') as string)

        this.port = url.port
        this.hostname = url.hostname
        this.protocols = config.get('ActivityPub.ID.HTTP.protocols', ['https', 'http']) as string[]
    }

    isThisServersID(id: GlobalID | URL | string): boolean {
        if (typeof id === 'string') {
            id = new URL(id)
        }
        
        if (id instanceof GlobalID) {
            id = new URL(id.toString())
        }

        if (!this.protocols.includes(id.protocol.slice(0, -1))) {
            return false
        }

        if (id.hostname !== this.hostname || id.port !== this.port) {
            return false
        }

        return true
    }
}