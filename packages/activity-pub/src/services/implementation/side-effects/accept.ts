import { CollectionService, GlobalID, LocalID } from "@d4rkio/activity-pub/src";
import { lazyInject, provideSingleton } from "@d4rkio/activity-pub/src/container";
import { getLogger } from "@d4rkio/activity-pub/src/logger";
import { ApRequest } from "@d4rkio/activity-pub/src/types/request";
import { ActivityStreamActivity, idEqual, Types } from "../../../utils/activity-stream";
import { GetService } from "../../get";
import { SideEffectHandler } from "../../side-effect";

const logger = getLogger('SideEffectService')

@provideSingleton(SideEffectHandler)
export class AcceptSideEffectHandler implements SideEffectHandler {
    readonly type: string = Types.Accept

    @lazyInject(GetService) private getService: GetService
    @lazyInject(CollectionService) private collectionService: CollectionService

    async handle(activtiy: ActivityStreamActivity, request: ApRequest): Promise<void> {
        if (activtiy.object.type === 'Follow' && request.federated === true) {
            return await this.handleAcceptFollow(activtiy, request)
        }

        logger.debug('did not handel accept activity')
    }

    async handleAcceptFollow(activtiy: ActivityStreamActivity, request: ApRequest): Promise<void> {
        const followActivity: ActivityStreamActivity = await this.getService.get(GlobalID.create(activtiy.object.id), request)

        if (idEqual(followActivity.actor, request.owner)) {
            logger.debug('did not handle accept activity. follow not from boxowner')
            return
        }

        if (idEqual(followActivity.object, activtiy.actor)    ) {
            logger.debug('did not handle accept activity. Accapting actor is not the one beeing followed')
            return
        }

        const followingCollectionId = (await this.getService.get(request.owner, request)).following

        const followerId = (followActivity.object.id || followActivity.object) as string

        await this.collectionService.push(LocalID.fromGlobalID(followingCollectionId as string), {id: followerId})
    }
}

@provideSingleton(SideEffectHandler)
export class TentativeAcceptSideEffectHandler extends AcceptSideEffectHandler {
    readonly type: string = Types.TentativeAccept
}