import { CollectionService, GlobalID, LocalID } from "@d4rkio/activity-pub/src";
import { lazyInject, provideSingleton } from "@d4rkio/activity-pub/src/container";
import { getLogger } from "@d4rkio/activity-pub/src/logger";
import { ApRequest } from "@d4rkio/activity-pub/src/types/request";
import { ActivityStreamObject, idEqual, Types } from "../../../utils/activity-stream";
import { DeliveryService } from "../../delivery";
import { GetService } from "../../get";
import { ObjectService } from "../../object";
import { SideEffectHandler } from "../../side-effect";

const logger = getLogger('SideEffectService')

@provideSingleton(SideEffectHandler)
export class FollowSideEffectHandler implements SideEffectHandler {
    readonly type: string = Types.Follow

    @lazyInject(GetService) private getService: GetService
    @lazyInject(CollectionService) private collectionService: CollectionService
    @lazyInject(DeliveryService) private deliveryService: DeliveryService
    @lazyInject(ObjectService) private objectService: ObjectService

    async handle(activtiy: ActivityStreamObject, request: ApRequest): Promise<void> {
        if (request.federated === false) {
            //todo handle later
            return
        }

        if (!idEqual(request.owner, activtiy.object as ActivityStreamObject)) {
            logger.debug('recived follow not intended for inbox owner', {activtiy, request})
            return
        }

        const actor = await this.getService.get(request.owner, request)
        const followid = LocalID.fromGlobalID(actor.followers as string)
        
        const accept = {
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "TentativeAccept",
            "to": activtiy.actor,
            "actor": request.owner.toString(),
            "object": activtiy.id
        }

        await Promise.all([
            this.collectionService.push(followid, {id: activtiy.actor as string}),
            this.objectService.create(accept).then(accept => this.deliveryService.deliver(accept, {}))
        ])

        logger.debug('handled follow activiy', {actor: activtiy.actor, id: activtiy.id, target: activtiy.object, collection: followid})
    }
}