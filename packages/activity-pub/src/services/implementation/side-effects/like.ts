import { lazyInject, provideSingleton } from "@d4rkio/activity-pub/src/container";
import { getLogger } from "@d4rkio/activity-pub/src/logger";
import { GlobalID, LocalID } from "@d4rkio/activity-pub/src/types";
import { ApRequest } from "@d4rkio/activity-pub/src/types/request";
import { ActivityStreamObject, idEqual, Types } from "../../../utils/activity-stream";
import { CollectionService } from "../../collection";
import { GetService } from "../../get";
import { SideEffectHandler, SideEffectOptions } from "../../side-effect";

const logger = getLogger('SideEffectService')

@provideSingleton(SideEffectHandler)
export class LikeSideEffectHander implements SideEffectHandler {
    readonly type: string = Types.Like

    @lazyInject(GetService) private getService: GetService
    @lazyInject(CollectionService) private collectionService: CollectionService
    
    async handle(activtiy: ActivityStreamObject, request: ApRequest): Promise<void> {
        if (! idEqual(activtiy.actor as ActivityStreamObject, request.identity)) {
            logger.debug(
                'Like activity not handled. This might be intantional, if it was recieved from a federated server',
                {identety: request.identity, id: activtiy.id, actor: activtiy.actor}
            )
            return
        }

        const actor = await this.getService.get(GlobalID.create(activtiy.actor as string), {identity: request.identity})
        const likedid = LocalID.fromGlobalID(actor.liked as string)

        await this.collectionService.push(likedid, {id: activtiy.id})
        logger.debug('handled like activiy', {actor: activtiy.actor, id: activtiy.id})
    }
}