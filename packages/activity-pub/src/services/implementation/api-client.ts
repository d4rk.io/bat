import { provideSingleton } from "../../container";
import { GlobalID } from "../../types";
import { ActivityStreamObject } from "../../utils/activity-stream";
import { ApiClientService } from "../api-client";
import fetch from 'node-fetch'

@provideSingleton(ApiClientService)
export class ConcreatApiClientService implements ApiClientService {
    async get(id: GlobalID): Promise<ActivityStreamObject> {
        const response = await fetch(id.toString())

        return await response.json()
    }

    async deliver(id: GlobalID, object: ActivityStreamObject): Promise<void> {
        const response = await fetch(id.toString(), {
            body: JSON.stringify(object),
            method: 'POST',
            headers: {
                'Content-Type': 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
            }
        })
    }
}