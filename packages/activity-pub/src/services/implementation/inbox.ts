import Conf from 'conf/dist/source';
import { inject, provideSingleton } from '../../container';
import { GlobalID, LocalID } from '../../types';
import { InvalidObjectAPError, NotFoundAPError, RequestError } from '../../utils';
import { ActivityStreamObject, idEqual, isActivity } from '../../utils/activity-stream';
import { CollectionService, InboxService, SideEffectService } from '..';
import { getLogger } from '../../logger';
import { ApRequest } from '../../types/request';

const logger = getLogger('InboxService')

@provideSingleton(InboxService)
export class ConcreatInboxService implements InboxService {
    @inject(CollectionService) private collectionService: CollectionService
    @inject(SideEffectService) private sideEffectService: SideEffectService
    
    IDprefix: string
    private baseUrl: string

    constructor(@inject('Conf') config: Conf) {
        this.IDprefix = config.get('ActivityPub.Services.InboxService.baseID') as string
        this.baseUrl = config.get('ActivityPub.ID.HTTP.url') as string
    }

    async create(ownerID: GlobalID): Promise<GlobalID> {
        return GlobalID.create(this.baseUrl + this.IDprefix + '/' + await this.collectionService.create(ownerID))
    }

    async get(inbox: LocalID, request: ApRequest): Promise<ActivityStreamObject> {
        request.federated = false

        if (request.identity === undefined) {
            throw new RequestError('Not Authorized', 401)
        }

        try {
            request.owner = await this.collectionService.getOwner(inbox)
        } catch(e) {
            if (! (e instanceof NotFoundAPError)) {
                throw e
            }

            throw new RequestError('Outbox not found', 404)
        }

        if (!idEqual(request.owner, request.identity)) {
            throw new RequestError('Not Allowed; Only the owner can acces its inbox', 405)
        }

        return await this.collectionService.get(inbox, request)
    }

    async receive(inbox: LocalID, object: ActivityStreamObject, request: ApRequest): Promise<GlobalID> {
        request.federated = true

        try {
            if(! isActivity(object)) {
                throw new RequestError('Only activies can be delivert to an inbox', 400)
            }

            await this.collectionService.push(inbox, object)
            request.owner = await this.collectionService.getOwner(inbox)

            this.sideEffectService.execute(object, request)

            logger.debug('recived activity', {reciver: inbox, sender: object.actor})

            return GlobalID.create(object.id)
        } catch(e) {
            if (e instanceof NotFoundAPError) {
                throw new RequestError('Outbox not found', 404)
            }

            if (e instanceof InvalidObjectAPError) {
                throw new RequestError('Invalid object', 400)
            }

            throw e
        }
    }
}