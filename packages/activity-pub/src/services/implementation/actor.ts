import { ActorService, ObjectService, InboxService, OutboxService } from '..'
import { GlobalID } from "../../types";
import { inject, provideSingleton } from "../../container";
import { CollectionService } from '../collection';
import Conf from 'conf';
import { getLogger } from '../../logger';

const logger = getLogger('ActroServices')


@provideSingleton(ActorService)
export class ConcreateActorService implements ActorService {
    @inject(ObjectService) private objectService: ObjectService
    @inject(InboxService) private inboxService: InboxService
    @inject(OutboxService) private outboxService: OutboxService
    @inject(CollectionService) private collectionService: CollectionService

    private IDprefix: string
    private baseUrl: string

    constructor(@inject('Conf') config: Conf) {
        this.IDprefix = config.get('ActivityPub.Services.CollectionService.baseID') as string
        this.baseUrl = config.get('ActivityPub.ID.HTTP.url') as string
    }

    async create(): Promise<GlobalID> {
        const actor = await this.objectService.create({
            "@context": "https://www.w3.org/ns/activitystreams",
            "type": "Person"
        })

        const actorID = GlobalID.create(actor.id)

        await Promise.all([
            this.collectionService.create(actorID).then(id => {
                actor.followers = GlobalID.create(this.baseUrl + this.IDprefix + '/' + id.toString()).toString()
            }),
            this.collectionService.create(actorID).then(id => {
                actor.liked = GlobalID.create(this.baseUrl + this.IDprefix + '/' + id.toString()).toString()
            }),
            this.collectionService.create(actorID).then(id => {
                actor.following = GlobalID.create(this.baseUrl + this.IDprefix + '/' + id.toString()).toString()
            }),
            this.outboxService.create(actorID).then(id => {
                actor.outbox = id.toString()
            }),
            this.inboxService.create(actorID).then(id => {
                actor.inbox = id.toString()
            })
        ])

        await this.objectService.update(actor)

        logger.info('created actor', {actor})

        return GlobalID.create(actor.id)
    }
}