import { DeliveryService } from "..";
import { activityStream, sleep } from '../../utils'
import Conf from "conf";
import { GlobalID } from "../../types";
import util from 'util'
import { inject, lazyInject, provideSingleton } from "../../container";
import { getLogger } from "../../logger";
import { ActivityStreamObject } from "../../utils/activity-stream";
import { ReceiveService } from "../recive";
import { GetService } from "../get";
import { IDService } from "../id";
import { DeliveryOptions } from "../delivery";
import { validateLocaleAndSetLanguage } from "typescript";

const logger = getLogger('DeliveryService')

@provideSingleton(DeliveryService)
export class ConcreatDeliveryService implements DeliveryService {
    @inject(IDService) private idService: IDService
    @lazyInject(ReceiveService) private receiveService: ReceiveService
    @lazyInject(GetService) private getService: GetService

    private maxTries: number
    private backoffTime: number

    constructor(@inject('Conf') config: Conf) {
        this.maxTries = config.get('ActivityPub.Services.DeliveryService.maxTries') as number
        this.backoffTime = config.get('ActivityPub.Services.DeliveryService.backoffTime') as number
    }

    async deliver(activty: ActivityStreamObject, options: DeliveryOptions = {}): Promise<void> {
        logger.debug('activty stagt for delivery', {id: activty.id})

        const audience = activityStream.getAudience(activty)

        //this can be a promis, callback is called potentoly multiple times
        //donot await send, send should run async, but test might want to call this sync
        await this.resolve(audience, async (inboxes) => {
            logger.debug('delivering activity', {to: inboxes, id: activty.id})

            //it would be more effictant to filter for local server in multiple locations in the resolve frunction. This could
            //reduce the number of useless resolves for collection delivery. But if would not work with ipfs actor objects.
            //For shared inboxes on ipfs actors not the location of the actor is importatent, but the location of its inbox.
            if (options.onlyLocalDelivery === true) {
                inboxes = inboxes.filter((id) => this.idService.isThisServersID(id))
            }

            await this.receiveService.receiveAll(inboxes, activty, {federated: true})
        })
    }

    //try to resolve audience: an audience id can be succesfuly resolved to an actor or inbox id or can not be resolved
    //try to resolve actor ids: an actor id can be successfuly resolved or not
    //async call the callback with all resolved inbox ids (from audience and form actor ids)
    //sleep backoff time * tries
    //repeat with unsuccesfully resolved ids (audince and actor ids), from loop one, until maxTries has been reached
    async resolve(audience: GlobalID[], callback: ((audience: GlobalID[]) => Promise<void>)): Promise<void[]> {
        const callbackPromises: Promise<void>[] = []

        let tries = 0
        while(audience.length > 0 && tries < this.maxTries) {
            const inboxes: GlobalID[] = []
            const actors: GlobalID[] = []

            const results = await this.getService.getAll(audience, {})
            audience = []

            //resolve all ids
            // possible outcomes:
            //      id not resolved retry
            //      id collection > list of actor ids
            //      id actor > inbox id
            for (const [id, result] of results) {

                if (result === undefined) {
                    audience.push(id)
                    continue
                }

                if (result.type === undefined) {
                    continue
                }
    
                if (activityStream.isActor(result) && result.inbox !== undefined) {
                    inboxes.push(GlobalID.create(result.inbox as string))
                    continue
                }
    
                if (activityStream.isCollection(result) && result.items !== undefined) {
                    for (const item of result.items as ActivityStreamObject[] | String[]) {
                        if (typeof item === 'string') {
                            actors.push(GlobalID.create(item))
                        } else {
                            const object = item as ActivityStreamObject //why dose type reducing work for if case but not for the else case, it cannot be a string
                            
                            if (object.id === undefined) {
                                continue
                            }

                            actors.push(GlobalID.create(object.id))
                        }
                    }
                    continue
                }
            }

            //resolve actor ids to inbox ids
            if (actors.length > 0) {
                const resultsActor = await this.getService.getAll(actors, {})
                for (const [id, result] of resultsActor) {
                    if (result === undefined) {
                        audience.push(id)
                        continue
                    }

                    if (result.type === undefined) {
                        continue
                    }
        
                    if (activityStream.isActor(result) && result.inbox !== undefined) {
                        inboxes.push(GlobalID.create(result.inbox as string))
                        continue
                    }
                }
            }

            if (inboxes.length > 0) {
                //inboxes are an object of type GlobalID, and js always uses object identiy as hash :(
                const uniqueInboxes = GlobalID.createMultiple(Array.from(new Set(inboxes.map(inbox => inbox.toString()))))

                callbackPromises.push(callback(uniqueInboxes))
            }

            tries += 1

            sleep(this.backoffTime * tries)
        }

        return await Promise.all(callbackPromises)
    }
}

export class SilentDropDeliveryService implements DeliveryService {
    async deliver(object: ActivityStreamObject): Promise<void> {}
}

export class LogDropDeliveryService implements DeliveryService {
    async deliver(object: ActivityStreamObject): Promise<void> {
        console.log(util.inspect(object))
    }
}