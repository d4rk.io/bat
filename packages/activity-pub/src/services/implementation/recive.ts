import { GlobalID, LocalID } from "../../types";
import path from 'path/posix'
import { RequestError } from "../../utils";
import { inject, lazyInject, provideSingleton } from "../../container";
import { ReceiveService, OutboxService, InboxService, ReceivableService } from "..";
import { ActivityStreamObject } from "../../utils/activity-stream";
import { ApRequest } from "../../types/request";
import { IDService } from "../id";
import { ApiClientService } from "../api-client";

@provideSingleton(ReceiveService)
export class ConcreatReceiveService implements ReceiveService {
    @inject(InboxService) private inboxService: ReceivableService
    @inject(IDService) private idService: IDService
    @lazyInject(ApiClientService) private apiClientService: ApiClientService
    @lazyInject(OutboxService) private outboxService: ReceivableService
    
    receive(id: GlobalID, activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID>;
    receive(prefix: string, id: LocalID, activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID>;
    async receive(prefixOrId: any, idOrActivity: any, activityOrRequest: any, request?: ApRequest): Promise<GlobalID> {
        if (prefixOrId instanceof GlobalID) {
            return await this.reseiveWithGlobalID(prefixOrId, idOrActivity, activityOrRequest)
        }

        if (typeof prefixOrId === 'string' && idOrActivity instanceof LocalID) {
            return await this.reseiveWithLocalID(prefixOrId, idOrActivity, activityOrRequest, request)
        }

        throw new Error("Unreachable Code");
    }

    async receiveAll(ids: GlobalID[], activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID[]> {
        const promises = []
        for (const id of ids) {
            promises.push(
                this.reseiveWithGlobalID(id, activity, request)
            )
        }

        return await Promise.all<GlobalID>(promises)
    }

    private async reseiveWithGlobalID(id: GlobalID, activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID> {
        const idAsUrl = new URL(id.toString())

        if (! this.idService.isThisServersID(idAsUrl)) {
            await this.apiClientService.deliver(id, activity)

            return undefined
        }

        const prefix = path.dirname(idAsUrl.pathname)
        const lid: LocalID = LocalID.fromGlobalID(id)

        return await this.reseiveWithLocalID(prefix, lid, activity, request)
    }

    async reseiveWithLocalID(prefix: string, lid: LocalID, activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID> {
        switch(prefix) {
            case this.inboxService.IDprefix:
                return await this.inboxService.receive(lid, activity, request)

            case this.outboxService.IDprefix:
                return await this.outboxService.receive(lid, activity, request)
        }

        throw new RequestError('prefix unknow', 500)
    }
}