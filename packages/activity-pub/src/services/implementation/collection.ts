import nano from 'nano';
import { InvalidObjectAPError, NotFoundAPError } from '../../utils';
import { GlobalID, LocalID } from '../../types/ids';
import { inject, provideSingleton, Symbols } from '../../container';
import Conf from 'conf';
import { Collection, CollectionService } from '..';
import { ActivityStreamObject } from '../../utils/activity-stream';
import { getLogger } from '../../logger';
import { ApRequest } from '../../types/request';

const logger = getLogger('CollectionService')

@provideSingleton(CollectionService)
export class ConcreatCollectionService implements CollectionService {
    dbClientProvider: Promise<nano.DocumentScope<Collection>>
    IDprefix: string

    constructor(@inject(Symbols.CouchDBClient) couchDBClient: nano.ServerScope, @inject('Conf') config: Conf) {
        const dbPrefix = config.get('ActivityPub.couchdb.prefix') as string
        const dbName = dbPrefix + config.get('ActivityPub.Services.CollectionService.dbName') as string
        this.dbClientProvider = this.initDbClient(couchDBClient, dbName)

        this.IDprefix = config.get('ActivityPub.Services.CollectionService.baseID') as string
    }

    async create(ownerId: GlobalID): Promise<LocalID> {
        const collection: Collection = {
            '@context': 'https://www.w3.org/ns/activitystreams',
            'type': 'Collection',
            'attributedTo': ownerId.toString(),
            'items': []
        }

        return await this.couchCreate(collection)
    }

    async push(collection: LocalID, object: ActivityStreamObject): Promise<void> {
        if (object.id === undefined) {
            throw new InvalidObjectAPError({missing: 'id'})
        }

        await this.couchPush(collection, object)

        logger.debug('pushed object to collection', {collection: collection, id: object.id})
    }

    //todo filter not implemented
    //todo no pramsa for paging
    async get(collection: LocalID, request: ApRequest): Promise<Collection> {
        const itemsPromise = this.couchViewItems(collection)
        const object = await this.couchViewMetadata(collection)
        object.items = await itemsPromise

        return object
    }

    async getOwner(collection: LocalID): Promise<GlobalID> {
        const metadata = await this.couchViewMetadata(collection)
        return GlobalID.create(metadata.attributedTo)
    }

    private async couchCreate(collection: Collection): Promise<LocalID> {
        const dbClient = await this.dbClientProvider
        const {id} = await dbClient.insert(collection)
        return LocalID.create(id)
    }

    private async couchPush(collection: LocalID, object: ActivityStreamObject): Promise<void> {
        const dbClient = await this.dbClientProvider
        const result = await dbClient.atomic('collections', 'pushItem', collection.toString(), object)
        if(result !== "updated") { //yes true with "". its a string
            throw new NotFoundAPError('collection')
        }
    }

    private async couchViewMetadata(collection: LocalID): Promise<Collection> {
        const dbClient = await this.dbClientProvider
        const doc = (await dbClient.view<Collection>('collections', 'viewMetadata', {key: collection.toString()}))
        if (doc.rows.length === 0) {
            throw new NotFoundAPError('collection')
        }

        const object = doc.rows[0].value
        delete object._rev
        delete object._id
        
        return object
    }

    private async couchViewItems(collection: LocalID, page: number = undefined, pageLength: number = undefined): Promise<ActivityStreamObject[]> {
        const dbClient = await this.dbClientProvider
        const params: nano.DocumentViewParams = {start_key: [collection.toString()], end_key: [collection.toString(), {}]}

        if (page !== undefined) {
            if (pageLength === undefined) {
                pageLength = 42
            }

            params.limit = pageLength
            params.skip = pageLength * page
        }

        const doc = await dbClient.view<ActivityStreamObject>('collections', 'viewItems', params)
        return doc.rows.map(r => r.value)
    }

    private async initDbClient(couchDBClient: nano.ServerScope, dbName: string): Promise<nano.DocumentScope<Collection>> {
        const dbClient = couchDBClient.db.use<Collection>(dbName)
 
        try {
            await dbClient.info()
        } catch {
            await couchDBClient.db.create(dbName)
        }

        const design: any = {
            _id: '_design/collections',
            views: {
                viewMetadata: {
                    map: 'function(doc) {var doc2 = JSON.parse(JSON.stringify(doc));delete doc2.items;emit(doc2._id, doc2)}'
                },
                viewItems: {
                    map: 'function(doc) {doc.items.forEach(function(item) {var item2 = JSON.parse(JSON.stringify(item));delete item2._pushedAt;emit([doc["_id"], item._pushedAt],item2)})}'
                }
            },
            updates: {
                pushItem: 'function(doc, req) {if(doc===null){return [null,"false"]}var object=JSON.parse(req.body);object._pushedAt=Date.now().toString();doc.items.push(object);return [doc, "updated"]}'
            }
        }
    
        try {
            const {_rev} = await dbClient.get(design._id)
            design._rev = _rev
        } catch (e) {
            if (!(e instanceof Error && e.message === 'missing')) {
                throw e
            }
        }
    
        await dbClient.insert(design)
        return dbClient
    }
}