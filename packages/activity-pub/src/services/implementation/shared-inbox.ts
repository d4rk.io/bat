import Conf from "conf";
import { inject, lazyInject } from "../../container";
import { ApRequest } from "../../types/request";
import { ActivityStreamObject } from "../../utils/activity-stream";
import { DeliveryService } from "../delivery";
import { SharedInboxService } from "../shared-inbox";

export class ConcreatSharedInboxService implements SharedInboxService {
    @lazyInject(DeliveryService) private deliveryService: DeliveryService

    readonly IDprefix: string;

    constructor(@inject('Conf') config: Conf) {
        this.IDprefix = config.get('ActivityPub.Services.SharedInboxService.baseID') as string
    }

    async receive(activity: ActivityStreamObject, request: ApRequest): Promise<void> {
        await this.deliveryService.deliver(activity, {onlyLocalDelivery: true})
    }
}