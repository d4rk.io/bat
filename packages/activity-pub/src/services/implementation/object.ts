import nano from "nano";
import Conf from 'conf'
import { InvalidObjectAPError, NotFoundAPError } from "../../utils";
import { GlobalID, LocalID } from "../../types";
import { inject, provideSingleton, Symbols } from "../../container";
import { ObjectService } from "..";
import { ActivityStreamObject } from "../../utils/activity-stream";


@provideSingleton(ObjectService)
export class ConcreatObjectService implements ObjectService{
    private dbClientProvider: Promise<nano.DocumentScope<ActivityStreamObject>>
    IDprefix: string
    private baseUrl: string


    constructor(@inject(Symbols.CouchDBClient) couchDBClient: nano.ServerScope, @inject('Conf') config: Conf) {
        const dbPrefix = config.get('ActivityPub.couchdb.prefix') as string
        const dbName = dbPrefix + config.get('ActivityPub.Services.ObjectService.dbName') as string
        this.dbClientProvider = this.initDBClient(couchDBClient, dbName)

        this.IDprefix = config.get('ActivityPub.Services.ObjectService.baseID') as string
        this.baseUrl = config.get('ActivityPub.ID.HTTP.url') as string
    }

    async update(object: ActivityStreamObject): Promise<ActivityStreamObject> {
        const id = object.id
        if (id === undefined) {
            throw new InvalidObjectAPError({missing: 'id'})
        }

        await this.couchUpdate(LocalID.fromGlobalID(id), object)
        return object
    }

    async create(object: ActivityStreamObject): Promise<ActivityStreamObject> {
        if (object.id !== undefined) {
            throw new InvalidObjectAPError({toMuch: 'id'})
        }

        return await this.createObjects(object)
    }

    //recursivly creates invidual objects
    //dupplicates object content in parent objects, because im to lazy to write a resolver for piecing the objects back to gather on get
    private async createObjects(object: ActivityStreamObject): Promise<ActivityStreamObject> {
        object = Object.assign({}, object); //shallow clone object

        const promises = []

        for (const key in object) {
            if (typeof object[key] !== 'object' || Array.isArray(object[key])) {
                continue
            }

            if ((object[key] as ActivityStreamObject).id !== undefined) {
                continue
            }

            promises.push(this.createObjects(object[key] as ActivityStreamObject).then((value => object[key] = value)))
        }

        await Promise.all(promises)

        const couchID = await this.couchPut(object)
        object.id = this.toGlobalID(couchID).toString()
        
        return object
    }

    async get(id: LocalID): Promise<ActivityStreamObject> {
        const object = await this.couchGet(id)

        object.id = this.toGlobalID(id).toString()
        
        return object
    }

    private toGlobalID(id: LocalID): GlobalID {
        return GlobalID.create(this.baseUrl + this.IDprefix + '/' + id)
    }

    private async couchPut(object: ActivityStreamObject): Promise<LocalID> {
        const {id} = await (await this.dbClientProvider).insert(object)
        return LocalID.create(id)
    }

    private async couchGet(id: LocalID): Promise<ActivityStreamObject> {
        let doc: ActivityStreamObject
        try {
            doc = await (await this.dbClientProvider).get(id.toString())
        } catch (e) {
            if (!(e instanceof Error && e.message === 'missing')) {
                throw e
            }

            throw new NotFoundAPError('object')
        }

        delete doc._id
        delete doc._rev

        return doc
    }

    private async couchUpdate(id: LocalID, object: ActivityStreamObject) {
        let rev: string
        try {
            const doc = await (await this.dbClientProvider).get(id.toString())
            rev = doc._rev
        } catch (e) {
            if (!(e instanceof Error && e.message === 'missing')) {
                throw e
            }

            throw new NotFoundAPError('object')
        }

        const obj: ActivityStreamObject = Object.assign({}, object)

        obj._id = id.toString()
        obj._rev = rev
        delete obj.id

        await (await this.dbClientProvider).insert(obj, {new_edits: false})
    }

    private async initDBClient(couchDBClient: nano.ServerScope, couchDBDatabaseName: string): Promise<nano.DocumentScope<ActivityStreamObject>> {
        const dbClient = couchDBClient.db.use<ActivityStreamObject>(couchDBDatabaseName)

        try {
            await dbClient.info()
        } catch {
            await couchDBClient.db.create(couchDBDatabaseName)
        }

        return dbClient
    }
}