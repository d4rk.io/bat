import { GlobalID, LocalID } from "../types";
import { ApRequest } from "../types/request";
import { ActivityStreamObject } from "../utils/activity-stream";


export const ReceiveService = Symbol.for('ReceiveService')
export interface ReceiveService {
    receiveAll(ids: GlobalID[], activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID[]>
    receive(id: GlobalID, activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID>
    receive(prefix: string, id: LocalID, activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID>
}

export interface ReceivableService {
    receive(id: LocalID, activity: ActivityStreamObject, request: ApRequest): Promise<GlobalID>
    IDprefix: string
}