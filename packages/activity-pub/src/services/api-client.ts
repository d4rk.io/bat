import { GlobalID } from "../types";
import { ActivityStreamObject } from "../utils/activity-stream";

export const ApiClientService = Symbol.for('ApiClientService') 
export interface ApiClientService {
    get(id: GlobalID): Promise<ActivityStreamObject>
    deliver(id: GlobalID, object: ActivityStreamObject): Promise<void>
}