import { GlobalID } from "../types"
import { ApRequest } from "../types/request"
import { ActivityStreamObject } from "../utils/activity-stream"

export interface SideEffectOptions {
    identety?: GlobalID
    boxOwner?: GlobalID
}

export const SideEffectService = Symbol.for('SideEffectService')
export interface SideEffectService {
    execute(object: ActivityStreamObject, request: ApRequest): Promise<void>
}

export const SideEffectHandler = Symbol.for('SideEffectHandler')
export interface SideEffectHandler {
    readonly type: string
    handle(activtiy: ActivityStreamObject, request: ApRequest): Promise<void>
}