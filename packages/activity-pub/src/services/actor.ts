import { GlobalID } from "../types";

export const ActorService = Symbol.for('ActorService') 
export interface ActorService {
    create(): Promise<GlobalID>
}