export * from './services'
export * from './utils'
export * from './types'

// config:
// 'ActivityPub.Services.ConcreatObjectService.couchDBDatabaseName', 'objects'
// 'ActivityPub.Api.baseURL', 'objects'
// 'ActivityPub.Api.Object.urlPath', 'objects'
// 'ActivityPub.Services.ConcreatCollectionService.couchDBDatabaseName', 'collections'