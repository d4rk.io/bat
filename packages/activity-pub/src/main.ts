import { config } from "./config"
import { app } from "./api/app"
import { getLogger } from "./logger"

process.setMaxListeners(0);

const logger = getLogger('main')

const port = new URL(config.get('ActivityPub.ID.HTTP.url')).port

app.listen(port, () => {
    logger.info(`listening on port ${port}`)

    logger.info(config.get('ActivityPub.couchdb.url'))
    logger.info(config.get('ActivityPub.ID.HTTP.url'))
})