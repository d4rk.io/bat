import winston from 'winston'

export const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.Console({
            format: winston.format.prettyPrint()
        })
    ]
})

export function getLogger(service: any): winston.Logger {
    return winston.createLogger({
        level: 'debug',
        defaultMeta: {service: service.toString()},
        transports: [
            logger
        ]
    })
}