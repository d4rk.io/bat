import "reflect-metadata"
import { Container, interfaces } from "inversify"
import getDecorators from "inversify-inject-decorators"
import { fluentProvide, buildProviderModule } from "inversify-binding-decorators"
import Conf from "conf";
import nano from "nano";

export { inject } from 'inversify'

export const provideSingleton = function(identifier: interfaces.ServiceIdentifier<any>, force?: boolean) {
    return fluentProvide(identifier).inSingletonScope().done(force);
};

export const Symbols = {
    CouchDBClient: Symbol.for('CouchDBClient')
}

export const container = new Container()

export const { lazyInject } = getDecorators(container);
export function bindContainer(config: Conf): Container {
    container.bind<Conf>('Conf').toConstantValue(config)

    const couchDBClient = nano(config.get('ActivityPub.couchdb.url') as string)
    container.bind<nano.ServerScope>(Symbols.CouchDBClient).toConstantValue(couchDBClient)

    container.load(buildProviderModule())

    return container
}