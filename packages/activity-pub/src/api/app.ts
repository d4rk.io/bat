import express from "express"
import { config } from "../config";
import { bindContainer } from "../container";
import { getLogger } from "../logger";
import { ActorService, CollectionService, GetableService, InboxService, ObjectService, OutboxService, ReceivableService} from "../services";
import { GlobalID, LocalID } from "../types";
import { ApRequest } from "../types/request";
import { ActivityStreamObject } from "../utils/activity-stream";
import { APError, RequestError } from "../utils/errors";
import { httpLogger } from "./httpLogger";
import { authMiddleware } from "./middleware";


const logger = getLogger('express')
const container = bindContainer(config)

function writeError(res: express.Response, e: Error | RequestError | APError) {
    if (e instanceof APError) {
        logger.debug('An APError occurred while http request handeling', e)
        res.status(400).end(e.message)
        return
    }

    if (e instanceof RequestError) {
        logger.debug('A RequestError occurred while http request handeling', e)
        res.status(e.statuscode).end(e.message)
        return
    }

    logger.error('An Error occurred while http request handeling', e)
    res.status(500).end(e.message)
    res.end()
}

function apRequestFrom(req: express.Request): ApRequest {
    const options: ApRequest = {
        identity: GlobalID.create(req.headers.identity as string)
    }
    return options
}

function addGetable(app: express.Express, service: any, ) {
    const prefix = container.get<GetableService>(service).IDprefix

    logger.debug('add route: GET :: ' + prefix + '/:id => ')

    app.get(prefix + '/:id', async (req: express.Request, res: express.Response) => {
        const lid = LocalID.create(req.params.id)
        const options = apRequestFrom(req)

        let object: ActivityStreamObject
        try {
            object = await container.get<GetableService>(service).get(lid, options)
        } catch (e) {
            return writeError(res, e)
        }

        res.setHeader('Content-Type', 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
        res.write(JSON.stringify(object))
        res.end()
    })
}

function addRecivable(app: express.Express, service: any) {
    const prefix = container.get<ReceivableService>(service).IDprefix

    logger.debug('add route: POST :: ' + prefix + '/:id')

    app.post(prefix + '/:id', async (req: express.Request, res: express.Response) => {
        const lid = LocalID.create(req.params.id)
        const options = apRequestFrom(req)

        const activity = req.body

        let id: GlobalID
        try {
            id = await container.get<ReceivableService>(service).receive(lid, activity, options)
        } catch (e) {
            return writeError(res, e)
        }

        res.status(201).setHeader('Location', id.toString()).end()
    })
}

function addDevApi(app: express.Express) {

    logger.info('adding dev api routes')

    app.post('/dev/register/:name', async (req: express.Request, res: express.Response) => {
        let id: GlobalID
        try {
            id = await container.get<ActorService>(ActorService).create()
        } catch (e) {
            return writeError(res, e)
        }

        res.status(201).setHeader('Location', id.toString()).end(id.toString())
    })
}

export const app = express()

app.use(express.json({type: 'application/ld+json'}))
app.use(authMiddleware)

app.use(httpLogger)

addGetable(app, CollectionService)
addGetable(app, ObjectService)
addGetable(app, InboxService)
addGetable(app, OutboxService)

addRecivable(app, InboxService)
addRecivable(app, OutboxService)

addDevApi(app)