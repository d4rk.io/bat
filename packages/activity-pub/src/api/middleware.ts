import express from "express";

export function authMiddleware(req: express.Request, res: express.Response, next: any) {
    delete req.headers.identity

    const authHeader = req.headers.authorization
    if (authHeader !== undefined) {
        const authHeaderParts = authHeader.split(' ')

        if (authHeaderParts[0] === 'id' && authHeaderParts[1] !== undefined) {
            req.headers.identity = authHeaderParts[1]
        }
    }

    next()
}


