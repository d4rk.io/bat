import morgan from 'morgan'
import json from 'morgan-json'
import { getLogger } from '../logger'

const logger = getLogger('http-access-log')

const format = json({
  method: ':method',
  url: ':url',
  status: ':status',
  contentLength: ':res[content-length]',
  responseTime: ':response-time',
  identity: ':req[identity]'
})

export const httpLogger = morgan(format, {
  stream: {
    write: (message) => {
      const {
        method,
        url,
        status,
        contentLength,
        responseTime,
        identity
      } = JSON.parse(message)

      logger.info(method + ' :: ' + url, {
        method,
        url,
        status: Number(status),
        contentLength,
        responseTime: Number(responseTime),
        identity
      })
    }
  }
})