import { create } from '../../src'

describe('mdag.put', () => {
    const ipfs = create({host: 'localhost', port: 5001, protocol: 'http'})

    describe('new dag node', () => {
        test('create with dag root value', async () => {
            const obj = {a: 1, b: {c: 3}}
            const cid = await ipfs.mdag.put(obj)

            expect((await ipfs.dag.get(cid)).value).toStrictEqual(obj)
        })

        test('create with dag root value, with overwrite dissabled', async () => {
            const obj = {a: 1, b: {c: 3}}
            const cid = await ipfs.mdag.put(obj, {overwrite: false})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual(obj)
        })

        test('create with path', async () => {
            const obj = {a: 1, b: {c: 3}}
            const cid = await ipfs.mdag.put(obj, {path: '/path/to/obj'})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({path: {to: {obj: obj}}})
        })

        test('create with path, with / at the end', async () => {
            const obj = {a: 1, b: {c: 3}}
            const cid = await ipfs.mdag.put(obj, {path: '/path/to/obj/'})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({path: {to: {obj: obj}}})
        })

        test('create with path, with createPartens disabled', async () => {
            const obj = {a: 1, b: {c: 3}}
            
            await expect(
                ipfs.mdag.put(obj, {path: '/path/to/obj', createParents: false})
            ).rejects.toThrow('Subpath dose not exist')
        })

        test('create with path, with overwrite dissabled', async () => {
            const obj = {a: 1, b: {c: 3}}
            const cid = await ipfs.mdag.put(obj, {path: '/path/to/obj', overwrite: false})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({path: {to: {obj: obj}}})
        })
    })

    describe('write into existing dag, one block', () => {
        test('overwrite root', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            const newObj = {d: {e: 4}, f: 5}
            const cid = await ipfs.mdag.put(newObj, {cid: c1})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual(newObj)
        })

        test('overwrite root, overwrite dissabled', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            const newObj = {d: {e: 4}, f: 5}

            await expect(
                ipfs.mdag.put(newObj, {cid: c1, overwrite: false})
            ).rejects.toThrow('Key already exists')
        })

        test('overwrite new path', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            const cid = await ipfs.mdag.put('newval', {cid: c1, path: '/b/e/f'})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {c: 3, e: {f: 'newval'}}})
        })

        test('overwrite new path, with overwrite dissabled, no overwrite', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            const cid = await ipfs.mdag.put('newval', {cid: c1, path: '/b/e/f', overwrite: false})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {c: 3, e: {f: 'newval'}}})
        })

        test('overwrite new path, with createParents dissabled', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            await expect(
                ipfs.mdag.put('newval', {cid: c1, path: '/b/e/f', createParents: false})
            ).rejects.toThrow('Subpath dose not exist')
        })

        test('overwrite existing key value', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            const cid = await ipfs.mdag.put('newval', {cid: c1, path: '/b/c'})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {c: 'newval'}})
        })

        test('overwrite existing key value, with overwrite dissabled', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            await expect(
                ipfs.mdag.put('newval', {cid: c1, path: '/b/c', overwrite: false})
            ).rejects.toThrow('Key already exists')
        })

        test('overwrite existing key object', async () => {
            const obj = {a: 1, b: {c: {d: 2, e: 3}}}
            const c1 = await ipfs.mdag.put(obj)

            const cid = await ipfs.mdag.put('newval', {cid: c1, path: '/b/c'})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {c: 'newval'}})
        })

        test('overwrite existing key object, with overwrite dissabled', async () => {
            const obj = {a: 1, b: {c: {d: 2, e: 3}}}
            const c1 = await ipfs.mdag.put(obj)

            await expect(
                ipfs.mdag.put('newval', {cid: c1, path: '/b/c', overwrite: false})
            ).rejects.toThrow('Key already exists')
        })
    })

    describe('write into existing dag, multiple block', () => {
        test('new path', async () => {
            const c2 = await ipfs.mdag.put({c: 2, d: {e: 3}})

            const obj: any = {a: 1, b: c2}
            const c1 = await ipfs.mdag.put(obj)

            const cid = await ipfs.mdag.put('newval', {cid: c1, path: '/b/d/z'})

            expect((await ipfs.dag.get(cid, {path: '/b'})).value).toStrictEqual({c: 2, d: {e: 3, z: 'newval'}})
            
            const {value: res} = await ipfs.dag.get(cid)
            //cid dose not need to be tested, if it would be wrong the expect aboved would have failed
            res.b = 'cidwashear'
            obj.b = 'cidwashear'
            expect(res).toStrictEqual(obj)
        })
    })

    describe('with global path', () => {
        test('ipfs path', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            const path = `/ipfs/${c1.toString()}/b/e/f`

            const cid = await ipfs.mdag.put('newval', {path})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {c: 3, e: {f: 'newval'}}})
        })

        test('ipns path', async () => {
            const obj = {a: 1, b: {c: 3}}
            const c1 = await ipfs.mdag.put(obj)

            const {name} = await ipfs.name.publish(c1)

            const path = `/ipns/${name}/b/e/f`

            const cid = await ipfs.mdag.put('newval', {path})

            expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {c: 3, e: {f: 'newval'}}})
        }, 100000)
    })
})