import { create } from '../../src'
import CID from 'cids'

describe('mdag.rm', () => {
    const ipfs = create({host: 'localhost', port: 5001, protocol: 'http'})

    test('rm single block dag, value', async () => {
        const c1 = await ipfs.mdag.put({a: 1, b: {c: {e: 3}, d: 4}})

        const cid = await ipfs.mdag.rm('/b/c/e', {cid: c1})

        expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {c: {}, d: 4}})
    })

    test('rm single block dag, object', async () => {
        const c1 = await ipfs.mdag.put({a: 1, b: {c: {e: 3, g: 5}, d: 4}})

        const cid = await ipfs.mdag.rm('/b/c', {cid: c1})

        expect((await ipfs.dag.get(cid)).value).toStrictEqual({a: 1, b: {d: 4}})
    })

    test('rm multi block dag', async () => {
        const c2 = await ipfs.mdag.put({h: {i: 1, j: 1}})
        const obj: any = {a: 1, b: {c: {e: 3, g: c2}, d: 4}}
        const c1 = await ipfs.mdag.put(obj)

        const cid = await ipfs.mdag.rm('/b/c/g/h/j', {cid: c1})

        expect((await ipfs.dag.get(cid, {path: '/b/c/g'})).value).toStrictEqual({h: {i: 1}})

        const {value: res} = await ipfs.dag.get(cid)
        delete res.b.c.g
        delete obj.b.c.g
        expect(res).toStrictEqual(obj)
    })

    test('rm multi block dag, delete no existet path', async () => {
        const c2 = await ipfs.mdag.put({h: {i: 1, j: 1}})
        const obj = {a: 1, b: {c: {e: 3, g: c2}, d: 4}}
        const c1 = await ipfs.mdag.put(obj)

        await expect(
            ipfs.mdag.rm('/b/c/g/h/x/z', {cid: c1})
        ).rejects.toThrow('Subpath dose not exist')
    })

    test('rm multi block dag, delete no existet path, optimistic', async () => {
        const obj2 = {h: {i: 1, j: 1}}
        const c2 = await ipfs.mdag.put(obj2)
        const obj1: any = {a: 1, b: {c: {e: 3, g: c2}, d: 4}}
        const c1 = await ipfs.mdag.put(obj1)

        const cid = await ipfs.mdag.rm('/b/c/g/h/x/z', {cid: c1, optimistic: true})

        expect((await ipfs.dag.get(cid, {path: '/b/c/g'})).value).toStrictEqual(obj2)
        
        const {value: res} = await ipfs.dag.get(cid)

        expect(res.b.c.g.toString()).toStrictEqual(obj1.b.c.g.toString())
        delete res.b.c.g
        delete obj1.b.c.g

        expect(res).toStrictEqual(obj1)
    })


})