'use strict'
/* eslint-env browser */

import type { Options } from 'ipfs-http-client/dist/src/lib/core'
import type { IPFS as IPFSCORE } from 'ipfs-core-types'
import type { EndpointConfig } from 'ipfs-http-client/dist/src/types'
import type { API } from './mdag'

import ipfsHttpClient from 'ipfs-http-client'

export {CID, multiaddr, multibase, multicodec, globSource, urlSource} from 'ipfs-http-client'


export type IPFS = IPFSCORE & { getEndpointConfig: () => EndpointConfig } & { mdag: API}

export function create (options: Options = {}) : IPFS {
  const client: any = ipfsHttpClient.create(options)

  client.key.import = require('./key/import')(options)
  client.mdag = require('./mdag').default(options)
  return client
}