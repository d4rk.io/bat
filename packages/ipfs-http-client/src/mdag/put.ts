'use strict'

import type CID from 'cids'
import type { PutOptions } from '.'

const configure = require('ipfs-http-client/src/lib/configure')

export default configure((api: any, opts: any) => {
    const getDag = require('ipfs-http-client/src/dag/get')(opts)
    const putDag = require('ipfs-http-client/src/dag/put')(opts)
    const getPathAndCid = require('./getPathAndCid').default(opts)
    const { CID } = require('ipfs-http-client')
    const { cloneDeep } = require('lodash')

    async function writeRecursive (cid: CID, path: string[], value: any, options: PutOptions) : Promise<CID> {
        // read dag root
        const {value: dag} = cid === null ? {value: {}}: await getDag(cid, options)

        // if no path is given, write root object
        if (path.length === 0) {
            // throw if overwrite is not enabled
            if (options.overwrite !== true && Object.keys(dag).length !== 0 && ! Object.is(dag, value)) {
                throw new Error('Key already exists')
            }

            //throw is root value is not an object
            if (typeof value !== 'object' && !(value instanceof CID)) {
                throw new Error('Root value must be an object or a CID.')
            }

            return await putDag(value, options)
        }

        // travers dag
        let current = dag
        while (path.length > 1) {
            const key = path.shift()!

            // if key dose not exist create an empty object
            if (current[key] === undefined) {
                if (options.createParents !== true) {
                    throw new Error('Subpath dose not exist')
                }

                current[key] = {}
            }

            // if key is a CID, recursivly call write on next block
            if (current[key] instanceof CID ) {
                current[key] = await writeRecursive(current[key], [...path], value, options)
                break
            }

            // if current key is not an object overwrite it
            if (typeof current[key] !== 'object') {
                if (options.overwrite !== true) {
                    throw new Error('Subkey already exists')
                }

                current[key] = {}
            }

            current = current[key]
        }

        // write value to key, if path end has ben reached
        if (path.length == 1) {
            const key = path[0]
            
            // throw if overwrite is not enabled 
            if (options.overwrite !== true 
                && current[key] !== undefined 
                && current[key] !== null 
                && (typeof current[key] !== 'object' || Object.keys(current[key]).length !== 0)
                && !Object.is(current[key], value)
            ) {
                throw new Error('Key already exists')
            }

            // write deepcopy to dag
            current[key] = cloneDeep(value)
        }
    
        // write updated dag to ipfs
        return await putDag(dag, options)
    }

    const put = async (value: any, options: PutOptions = {}) : Promise<CID> => {
        options.overwrite = options.overwrite === undefined ? true : options.overwrite
        options.createParents = options.createParents === undefined ? true : options.createParents
        
        const {path, cid} = await getPathAndCid(options.path, options.cid)

        // delete options, that should not be set for put and get
        delete options.path
        delete options.cid

        return await writeRecursive(cid, path, value, options)
    }

    return put
})