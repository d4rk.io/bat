'use strict'

import type { AbortOptions, PreloadOptions } from 'ipfs-core-types/src/utils'
import type { GetResult } from 'ipfs-core-types/src/dag'
import type CID from 'cids'
import type { CIDVersion } from 'cids'
import type { CodecName } from 'multicodec'
import type { HashName } from 'multihashes'
import type { Options } from 'ipfs-http-client/dist/src/types'

export interface API<OptionExtension = {}> {
    get: (path: string, options: GetOptions & OptionExtension) => Promise<GetResult>

    put: (node: any, options?: PutOptions & OptionExtension) => Promise<CID>

    rm: (path: string, options?: RmOptions & OptionExtension) => Promise<CID>
}

export interface GetOptions extends AbortOptions, PreloadOptions {
    cid?: CID
    localResolve?: boolean
}

export interface PutOptions extends AbortOptions, PreloadOptions {
    path?: string
    cid?: CID
    format?: CodecName
    hashAlg?: HashName
    version?: CIDVersion
    pin?: boolean
    onlyHash?: boolean
    overwrite?: boolean
    createParents?: boolean
}

export interface RmOptions extends AbortOptions, PreloadOptions {
    cid?: CID
    optimistic?: boolean
    format?: CodecName
    hashAlg?: HashName
    version?: CIDVersion
    pin?: boolean
    onlyHash?: boolean
}

export default (config: Options) : API => ({
  put: require('./put').default(config),
  get: require('./get').default(config),
  rm: require('./rm').default(config),
})