import type CID from 'cids'
import type { GetOptions } from './index'
import type { GetResult } from 'ipfs-core-types/src/dag'

const configure = require('ipfs-http-client/src/lib/configure')

export default configure((api: any, opts: any) => {
    const getPathAndCid = require('./getPathAndCid').default(opts)
    const dagGet = require('ipfs-http-client/src/dag/get')(opts)

    async function get(path: string, options?: GetOptions) : Promise<GetResult> {
        const {path: pathParts, cid}: {path: string[], cid: CID} = await getPathAndCid(path, options.cid)
        const localPath = '/' + pathParts.join('/')

        return dagGet(cid, {path: localPath})
    }

    return get
})