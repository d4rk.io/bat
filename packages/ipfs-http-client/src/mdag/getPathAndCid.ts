import type CID from 'cids'

const configure = require('ipfs-http-client/src/lib/configure')

export interface getPathAndCidReturn {
    path: string[]
    cid: CID
}

export default configure((api: any, opts: any) => {
    const resolve = require('ipfs-http-client/src/resolve')(opts)
    const { CID } = require('ipfs-http-client')

    const getPathAndCid = async (pathRaw: string, cidRaw: CID) : Promise<getPathAndCidReturn> => {
        let cid = null
        let path: string[] = []

        //parse path
        if (pathRaw !== undefined && path !== []) {
            path = pathRaw.split('/')
            if (path[0] !== '') {
                throw new Error('Path needs to start with /')
            }
            path.shift()

            //path is an ipfs path => set cid
            //@ts-ignore what ???
            if (path[0] === 'ipfs') {
                cid = new CID(path[1])

                path.shift()
                path.shift()

            //path is an ipns path => resolve => set cid
            //@ts-ignore what ???
            } else if (path[0] === 'ipns') {
                cid = new CID((await resolve(`/ipns/${path[1]}`)).split('/')[2])

                path.shift()
                path.shift()
            }

            //else path is a local path from a cid, or from a new root

            //remove last element if empty, path ended with /
            if (path.length > 0 && path[path.length-1] === '') {
                path.pop()
            }
        }

        //set cid if present and not already set by path
        if (cidRaw !== undefined && cid === null) {
            cid = cidRaw
        }

        return {path, cid}
    }

    return getPathAndCid
})