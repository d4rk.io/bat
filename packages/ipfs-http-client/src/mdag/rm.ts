'use strict'

import type CID from 'cids'
import type { RmOptions } from '.'

const configure = require('ipfs-http-client/src/lib/configure')

export default configure((api: any, opts: any) => {
    const getDag = require('ipfs-http-client/src/dag/get')(opts)
    const putDag = require('ipfs-http-client/src/dag/put')(opts)
    const getPathAndCid = require('./getPathAndCid').default(opts)
    const { CID } = require('ipfs-http-client')

    async function deleteRecursive (cid: CID, path: string[], options: RmOptions) : Promise<CID> {
        // read dag root
        const {value: dag} = cid === null ? {value: {}}: await getDag(cid, options)

        // travers dag
        let current = dag
        while (path.length > 1) {
            const key = path.shift()!

            // if key dose not exist create an empty object
            if (current[key] === undefined) {
                if (options.optimistic !== true) {
                    throw new Error('Subpath dose not exist')
                }

                break
            }

            // if key is a CID, recursivly call write on next block
            if (current[key] instanceof CID ) {
                current[key] = await deleteRecursive(current[key], [...path], options)
                break
            }

            // if current key is not an object overwrite it
            if (typeof current[key] !== 'object') {
                throw new Error('Subnode on path is not an object, path invalid')
            }

            current = current[key]
        }

        // ip path end has been reached
        if (path.length === 1) {
            const key = path.shift()!
            delete current[key]
        } else if (path.length === 0) {
            throw 'this throw should no be rachable'
        }
    
        // write updated dag to ipfs
        return await putDag(dag, options)
    }

    const rm = async (path: string, options: RmOptions = {}) : Promise<CID> => {       
        options.optimistic = options.optimistic === undefined ? false : options.optimistic

        const {path: pathParts, cid} = await getPathAndCid(path, options.cid)

        if (cid === null) {
            throw 'No dag given, can not delte from a new dag'
        }

        if (pathParts.length === 0) {
            throw 'cannot delete root object'
        }

        // delete options, that should not be set for put and get
        //@ts-ignore
        delete options.path
        delete options.cid

        return await deleteRecursive(cid, pathParts, options)
    }

    return rm
})