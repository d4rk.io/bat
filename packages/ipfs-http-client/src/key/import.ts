'use strict'

const toCamel = require('ipfs-http-client/src/lib/object-to-camel')
const multipartRequest = require('ipfs-http-client/src/lib/multipart-request')
const configure = require('ipfs-http-client/src/lib/configure')
const toUrlSearchParams = require('ipfs-http-client/src/lib/to-url-search-params')
const abortSignal = require('ipfs-http-client/src/lib/abort-signal')
import { AbortController } from 'native-abort-controller'

import type { AbortOptions } from 'ipfs-core-types/src/utils'
import type { Key } from 'ipfs-core-types/src/key/index'
import type { HTTPClientExtraOptions } from 'ipfs-http-client/src/types'

module.exports = configure((api: any) => {
  async function importKey (name: string, pem: string, password: string, options: AbortOptions & HTTPClientExtraOptions = {}) : Promise<Key> {

    // allow aborting requests on body errors
    const controller = new AbortController()
    // @ts-ignore
    const signal = abortSignal(controller.signal, options.signal)

    const res = await api.post('key/import', {
      timeout: options.timeout,
      signal: signal,
      searchParams: toUrlSearchParams({
        arg: name,
        pem,
        password,
        ...options
      }),
      ...(
        // @ts-ignore
        await multipartRequest(pem, controller, options.headers)
      )
    })
    const data = await res.json()

    // @ts-ignore server output is not typed
    return toCamel(data)
  }
  return importKey
})
