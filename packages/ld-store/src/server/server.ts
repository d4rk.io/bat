import type { Provider } from './provider'

import { HttpServerProvider } from './provider'

const providerClasses: any = [HttpServerProvider]

export class Server {
    providers: Provider[] = []

    constructor(dependencies) {
        for (let provider of providerClasses) {
            this.providers.push(new provider(dependencies))
        }
    }

    run(): void {
        for (let provider of this.providers) {
            provider.run()
        }
    }

    async stop(): Promise<void> {
        const pormmises = []

        for (let provider of this.providers) {
            pormmises.push(provider.stop())
        }

        for (let prommis of pormmises) {
            await prommis
        }
    }
}