export interface Provider {
    run(): void
    stop(): Promise<void>
}