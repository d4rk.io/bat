import type { Provider } from './provider';

import http from 'http'
import fs from 'fs'
import ospath from 'path'
import httpTerminator from 'http-terminator';

export class HttpServerProvider implements Provider {
    tmpPath: string
    port: string
    httpServer: http.Server
    httpTerminator: httpTerminator.HttpTerminator

    constructor({config}) {
        this.tmpPath = config.get('ldStore.httpProvider.tmpPath', '/tmp/ldHttpProvider')
        this.port = config.get('ldStore.httpProvider.port', '8073')

        if (!fs.existsSync(this.tmpPath)){
            fs.mkdirSync(this.tmpPath);
        }

        this.httpServer = http.createServer((req, res) => {
            if (ospath.posix.dirname(req.url) !== '/') {
                res.statusCode = 403
                res.end()
                return
            }

            const path = this.tmpPath + '/' + req.url

            if (! fs.existsSync(path)) {
                res.statusCode = 404
                res.end()
                return
            }

            fs.readFile(path, (err, data) => {
                if (err) {
                    res.statusCode = 500
                    res.end()

                    console.warn(err)
                    return
                }

                res.statusCode = 200
                res.setHeader('Content-Type', 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"')
                res.end(data)

                console.debug('HttpServerProvider served ' + req.url)
            })
        })

        this.httpTerminator = httpTerminator.createHttpTerminator({server: this.httpServer})
    }
    
    run() {
        this.httpServer.listen(this.port, () => {
            console.log('HttpServerProvider listenting on ' + this.port)
        })
    }

    async stop() {
        await this.httpTerminator.terminate()
    }
}