import { HttpClientProvider, IpfsImmutableClientProvider, IpfsMutableClientProvider, Provider } from './provider'

const providerClasses: any[] = [HttpClientProvider, IpfsImmutableClientProvider, IpfsMutableClientProvider]

export class Client {
    providers: Map<string, Provider> = new Map<string, Provider>()

    constructor(dependencies: any) {
        for (const providerClass of providerClasses) {
            const provider: Provider = new providerClass(dependencies)
        
            if (provider.scheme === undefined) {
                throw 'something dose not work'
            }

            this.providers.set(provider.scheme, provider)
        }
    }

    async get(id: string, options: any = {}): Promise<any> {
        const uri = new URL(id)
        const provider = this.providers.get(uri.protocol.slice(0, -1))
        if (provider === undefined) {
            throw 'Scheme not supported, by get'
        }

        return provider.get(id, options)
    }

    async create(object: any, options: any = {}): Promise<string> {
        const {scheme = 'http'} = options

        const provider = this.providers.get(scheme)
        if (provider === undefined) {
            throw `Scheme ${scheme} not supported, by create`
        }

        if (provider.abilities.create !== true) {
            throw 'Scheme dose not support creation'
        }

        return provider.create(object, options)
    }

    async update(object: any, options: any = {}): Promise<any> {
        const id = object['@id']
        if (id === undefined) {
            throw 'Can not update object without id'
        }

        const uri = new URL(id)
        const provider = this.providers.get(uri.protocol.slice(0, -1))
        if (provider === undefined) {
            throw 'Scheme not supported, by update'
        }

        if (provider.abilities.create !== true) {
            throw 'Scheme dose not support mutation'
        }

        return provider.update(object, options)
    }

    async initUser(options: any): Promise<void> {
        for (const provider of Object.values(this.providers)) {
            await provider.initUser(options)
        }
    }
}