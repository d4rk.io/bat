import type { Provider } from "./provider";

import { IpfsClientProvider } from "./ipfs";
import isIPFS from 'is-ipfs'
import { nanoid } from 'nanoid'
import { HTTPError } from 'ipfs-utils/src/http.js'

export class IpfsMutableClientProvider extends IpfsClientProvider implements Provider {
    scheme: string = 'ipfsmutable'
    abilities: {mutable: boolean, create: boolean} = {mutable: true, create: true}

    async get(id: string, options: any): Promise<any> {
        const path = new URL(id).pathname

        if (!isIPFS.ipnsPath(path)) {
            throw `Path ${path} is invalid`
        }

        //read object from ipfs dag
        const {value} = await this.ipfs.mdag.get(path, {})

        //set id
        value['@id'] = id

        return value
    }

    async write(object: any, kid: string, path: string = null) {
        //deep copy
        const obj = Object.assign({}, object)

        //delete id, it should not be save with the object
        delete obj['@id']

        // genertate a new uniqide key under a keyhash (kid), if path not given
        if (path === null) {
            const uuid = nanoid()
            path = `/ipns/${kid}/${uuid}`
        }

        // put object in mdage at path // dose read before write
        const cid = await this.ipfs.mdag.put(obj, {path})

        // publish new version of mdag with key
        await this.ipfs.name.publish(cid, {key: kid})

        return path
    }

    async create(object: any, options: {key: string}): Promise<string> {
        if (typeof options.key !== 'string' || object.key === '') {
            throw 'IpfsMutableClientProvider.create requires a keyhash. {key: "keyhash"}'
        }

        const path = await this.write(object, options.key)
        
        return `${this.scheme}:${path}`
    }

    async update(object: any, options: any): Promise<any>{
        const id = object['@id']
        if (id === undefined) {
            throw 'Error: object has no id'
        }

        //read ipns path from id
        const path = new URL(id).pathname
        if (!isIPFS.ipnsPath(path)) {
            throw ('Error: "${path}" is not a valid ipns path')
        }

        // get keyhash and uuid from path / id
        const pathParts = path.split('/')
        const keyhash = pathParts[2]
        if (keyhash === '') {
            throw(`Keyhash should not be empty in ${path}`)
        }
        const uuid = pathParts[3]
        if (uuid === '') {
            throw(`uuid should not be empty in ${path}`)
        }

        //check if keyhash is installed in ipfs deamon
        if (await this.ipfs.key.list().then(a => a.filter(e => e.id == keyhash).length) == 0) {
            throw('keyhash ${keyhash} not installed in ipfs deamon')
        }

        await this.write(object, keyhash, path)

        return object
    }

    async initUser({key}: {key: {pem: string, kid: string}}): Promise<void> {
        const pkey = Buffer.from(key.pem, 'base64')

        let kid: string = key.kid
        if (await this.ipfs.key.list().then(a => a.filter(e => e.name == kid).length) == 0) {
            const {id} = await await this.ipfs.key.import(kid, Buffer.from(key.pem, 'base64').toString(), undefined)
            kid = id
        }

        //create and publish base dir if it dose not exist
        try {
            //throws if not resolvable
            await this.ipfs.resolve(`/ipns/${kid}`)
        } catch (e) {
            if (! (e instanceof HTTPError)) {
                throw e
            }

            //mmmmmmme put error codes in your api !!!!!!!
            if (e.message !== 'could not resolve name') {
                throw e
            }

            // publish empty node becouse ipns path dose not exist
            const cid = await this.ipfs.mdag.put({})
            await this.ipfs.name.publish(cid, {key: kid})
        }
    }
}