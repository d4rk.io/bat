import type { Provider } from "./provider";

import { IpfsClientProvider } from "./ipfs";
import isIPFS from 'is-ipfs'

export class IpfsImmutableClientProvider extends IpfsClientProvider implements Provider {
    scheme: string = 'ipfsimmutable' 
    abilities: {mutable: boolean, create: boolean} = {mutable: false, create: true}

    async get(id: string, options: any): Promise<any> {
        const path = new URL(id).pathname

        if (!isIPFS.path(path)) {
            throw ('Error: "${path}" is not a valid ipfs path')
        }

        // read file chunk by chunk
        const chunks = []
        for await (const chunk of this.ipfs.cat(path)) {
            chunks.push(chunk)
        }

        // converte data to js object
        const json = Buffer.concat(chunks).toString()
        const object = JSON.parse(json)

        //objects are store without there id set, it needs to be set on read
        object['@id'] = id
        return object
    }

    async create(object: any, options: any): Promise<string> {
        //shallow copy
        object = Object.assign({}, object)

        //object should not contain its id
        delete object['@id']
        
        //create ipfs file for object
        const json = JSON.stringify(object)
        const {cid} = await this.ipfs.add(json)

        return `${this.scheme}:/ipfs/${cid.toString()}`
    }

    async update(object: any, options: any): Promise<any>{
        throw 'Not Supported'
    }

    async initUser(options: any): Promise<void> {
        //no init needed
    }
}