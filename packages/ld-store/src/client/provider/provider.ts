export interface Provider {
    scheme: string;
    abilities: {mutable: boolean, create: boolean};
    get(id: string, options: any): Promise<any>
    create(object: any, options: any): Promise<string>
    update(object: any, options: any): Promise<any>
    initUser(options: any): Promise<void>
}