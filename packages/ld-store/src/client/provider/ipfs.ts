import type { IPFS } from '@d4rkio/ipfs-http-client'

import { create } from '@d4rkio/ipfs-http-client'

export class IpfsClientProvider {
    ipfs: IPFS

    constructor({config}) {
        const apiUrl = config.get('ldStore.ipfsImutableProvider.apiUrl', 'http://localhost:5001')

        this.ipfs = create(apiUrl)
    }
}