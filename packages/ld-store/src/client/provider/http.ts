import type { Provider } from './provider'

import ospath from 'path'
import fs from 'fs'
import { nanoid } from 'nanoid'


export class HttpClientProvider implements Provider {
    scheme: string = 'http' 
    abilities: {mutable: boolean, create: boolean} = {mutable: true, create: true}

    tmpPath: string
    url: string

    constructor({config}) {
        this.tmpPath = config.get('ldStore.httpProvider.tmpPath', '/tmp/ldHttpProvider')
        const hostname = config.get('ldStore.httpProvider.hostname', 'localhost')
        const port = config.get('ldStore.httpProvider.port', '8073')
        this.url = `${this.scheme}://${hostname}:${port}/`

        if (!fs.existsSync(this.tmpPath)){
            fs.mkdirSync(this.tmpPath);
        }
    }

    async get(id: string, options: any): Promise<any> {
        const uri = new URL(id)
        if (!uri.pathname) {
            throw 'Could not read path: ' + id
        }

        if (ospath.posix.dirname(uri.pathname) !== '/') {
            throw 'Invalied id, objects path has to start with /'
        }

        const path = this.tmpPath + '/' + ospath.posix.basename(uri.pathname)
        if (!fs.existsSync(path)){
            throw "Object dose not exist on the file system"
        }

        const content = fs.readFileSync(path, 'utf-8')

        return JSON.parse(content);
    }

    async create(object: any, options: any): Promise<string> {
        object = Object.assign({}, object)
        const uuid = nanoid()
        const path = this.tmpPath + '/' + uuid
        const id = this.url + uuid

        object['@id'] = id
        const json = JSON.stringify(object)

        fs.writeFileSync(path, json, 'utf-8')

        return id
    }

    async update(object: any, options: any): Promise<any>{
        const uri = new URL(object['@id'])
        if (!uri.pathname) {
            throw 'Could not read path: ' + object['@id']
        }

        if (ospath.posix.dirname(uri.pathname) !== '/') {
            throw 'Invalied id, objects path has to start with /'
        }

        const path = this.tmpPath + '/' + ospath.posix.basename(uri.pathname)
        if (!fs.existsSync(path)){
            throw "Object dose not exist on the file system"
        }

        const json = JSON.stringify(object)

        fs.writeFileSync(path, json, 'utf-8')

        return object
    }

    async initUser(options: any): Promise<void> {
        //no init needed
    }
}