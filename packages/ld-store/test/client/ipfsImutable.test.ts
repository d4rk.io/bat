import {LdStoreClient} from '../../src'
import Conf from 'conf'

const config = new Conf()
const dependencies = {
    config: config
}

test('LdStoreClient create, get', async () => {
    const lds = new LdStoreClient(dependencies)

    const big = new Array(40000 + 1).join('A');

    let object = {empty:'obj', biggerThanABlock: big}
    const id = await lds.create(object, {scheme: 'ipfsimmutable'})
    expect(new URL(id).protocol).toStrictEqual('ipfsimmutable:')

    const result = await lds.get(id)
    object['@id'] = id
    expect(result).toStrictEqual(object)
})