import {LdStoreClient} from '../../src'
import Conf from 'conf'

const config = new Conf()
const dependencies = {
    config: config
}

test('LdStoreClient', async ()  => {
    const lds = new LdStoreClient(dependencies)

    const key = {pem: 'CAESQC0lEiD6BbCqzDoyMrY8mu82giWFNqNqQKoES9FP9gW38AlpPwkpaKO1k9sq0L/0lhUFnRqUzt4cJO6IXpj/aw8=', kid: 'k51qzi5uqu5dm5yqnups0sc4d8iaydypze5ntrrt8t3vsub372hrtwlczz8emn'}
    const {kid: keyhash} = key

    lds.initUser({key})

    const big = new Array(4000 + 1).join('A')
    let object = {empty:'obj', biggerThanABlock: big}

    const id = await lds.create(object, {scheme: 'ipfsmutable', key: keyhash})
    expect(new URL(id).protocol).toStrictEqual('ipfsmutable:')

    const result = await lds.get(id)
    object['@id'] = id
    expect(result).toStrictEqual(object)

    object['key'] = 'value'
    //lds.update(object)) retruns object
    expect(await lds.update(object)).toStrictEqual(object)

    const res = await lds.get(id)
    expect(res).toStrictEqual(object)
}, 300000)