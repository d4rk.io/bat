import {LdStoreClient, LdStoreServer} from '../../src'
import fetch from 'node-fetch'
import Conf from 'conf'

const config = new Conf()
const dependencies = {
    config: config
}

test('LdStoreClient create, get, update, get', async () => {
    const lds = new LdStoreClient(dependencies)

    let object = {empty:'obj'}
    const id = await lds.create(object)
    expect(new URL(id).protocol).toStrictEqual('http:')

    const result = await lds.get(id)
    object['@id'] = id
    expect(result).toStrictEqual(object)

    object['key'] = 'value'
    expect(await lds.update(object)).toStrictEqual(object)

    expect(await lds.get(id)).toStrictEqual(object)
})

test.skip('LdStoreServer', async () => {
    const ldss = new LdStoreServer(dependencies)
    ldss.run()

    const lds = new LdStoreClient(dependencies)

    let object = {empty:'obj'}

    const id = await lds.create(object)
    object['@id'] = id

    fetch(id).then(res => res.json()).then(json => console.log(json))

    await ldss.stop()
})